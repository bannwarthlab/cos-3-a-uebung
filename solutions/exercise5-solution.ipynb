{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 5\n",
    "\n",
    "---\n",
    "Submission until 19/11/2024 12:00 p.m.\n",
    "\n",
    "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Solvation free energy\n",
    "\n",
    "The goal of this exercise will be to improve our understanding of the continuum solvation models, in particular,\n",
    "the (generalized) Born model.\n",
    "We will calculate the free energy of solvation for different molecules in different solvents using the generalized Born solvation model.\n",
    "Furthermore, we will compute the vaporization enthalpies for water and\n",
    "we will compare the obtained result with reference values from experiments.\n",
    "\n",
    "### Continuum solvation models\n",
    "\n",
    "A continuum solvation model considers the solvent as a uniform polarizable medium with a specific dielectric constant.\n",
    "Hence, the solute is placed in an appropriately shaped cavity in this medium.\n",
    "\n",
    "The free energy of solvation can be divided into 4 different contributions:\n",
    "\n",
    "$$\n",
    "\\delta G_{solv} = \\delta G_{cavity} + \\delta G_{elec} + \\delta G_{vdW} + W_{vol}\n",
    "$$\n",
    "\n",
    "$\\delta G_{cavity}$ is the free energy cost, required to \"dig\" a cavity in the solvent in which the solute remains.\n",
    "\n",
    "$\\delta G_{elec}$ is the term that refers to the change in the electrostatic interactions of the solute due to solvation.\n",
    "This includes interactions with the solvent molecules, but also changed interactions within the solute.\n",
    "In a continuum model, the inner cavity is filled by the solute which is assumed to have a\n",
    "relative permittivity $\\varepsilon_r=1$, while it is a different solvent-specific value for the solvent medium.\n",
    "\n",
    "$\\delta G_{vdW}$ is the third term that describes all changed noncovalent interactions (Pauli repulsion and London dispersion) that are not included in $\\delta G_{elec}$.  \n",
    "\n",
    "The last term $W_{vol}$, corresponds to the volume reduction during the change of the standard state.\n",
    "For every species, this is a constant contribution and affects relative free energies for processes with changing number\n",
    "of particles.\n",
    "\n",
    "Many models consider $\\delta G_{cavity}$ and $\\delta G_{vdW}$ in one simplified term, which is obtained by the solvent accessible surface area (SASA): \n",
    "\n",
    "$$\n",
    "\\delta G_{SASA} ≈ \\delta G_{cavity} + \\delta G_{vdW} \n",
    "$$\n",
    "\n",
    "giving: \n",
    "\n",
    "\\begin{equation}\n",
    "\\delta G_{solv} ≈ \\delta G_{SASA} + \\delta G_{elec} + W_{vol} \n",
    "\\end{equation}\\tag{1}\n",
    "\n",
    "\n",
    "$\\delta G_{SASA}$ contains empirical terms, and these will be provided for every free energy calculation in this exercise. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "57vzlqbm2RBL"
   },
   "source": [
    "### Task 1\n",
    "\n",
    "In this task, we will calculate the free energy of solvation for different ions, namely $\\mathrm{Li}^+$, $\\mathrm{Na}^+$ $\\mathrm{Cl}^-$. \n",
    "In addition, the solvation free energies of $\\mathrm{CO}$, $\\mathrm{H_2O}$ and $\\mathrm{Glucose}$ in different solvents will be computed. \n",
    "\n",
    "For the electrostatic contributions in equation 1, we will use the generalized Born model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "4S7N8QX_2RBM"
   },
   "outputs": [],
   "source": [
    "#Use scipy.constants for all elementary constants as in previous exercises\n",
    "import scipy.constants as sc\n",
    "\n",
    "import numpy as np \n",
    "\n",
    "#Relative perimittivities of solvents, epsilon_r: water, toluene\n",
    "epsilon_h2o = 78.3 \n",
    "epsilon_tol = 2.4\n",
    "epslion_thf = 7.25\n",
    "epsilon_mecn = 37.5\n",
    "\n",
    "\n",
    "R = sc.R # SI units: m^3 Pa K^-1 mol^-1 \n",
    "e = sc.e # au to Coulomb conversion factor \n",
    "epsilon0 = sc.epsilon_0 # Vacuum permitivity\n",
    "\n",
    "\n",
    "#Generalized Born parameters in Angstrom\n",
    "gb_param = {\n",
    "    'h': 0.850,\n",
    "    'li': 0.144,\n",
    "    'c': 0.445,\n",
    "    'o': 0.500,\n",
    "    'na': 0.186,\n",
    "    'cl': 0.181\n",
    "}\n",
    "\n",
    "\n",
    "### Free solvation energy values as reference value for checking: \n",
    "# Minessota solavtion Database (see lecture slides): https://comp.chem.umn.edu/mnsol/\n",
    "# Other source: https://link.springer.com/article/10.1007/s00214-012-1250-7 \n",
    "\n",
    "# water_in_water   = -26.4 kJ/mol\n",
    "# water_in_toluene = -7.07 kJ/mol\n",
    "# Li_in_water = -513.4 kJ/mol\n",
    "# Li_in_acetonitrile = -537.2 kJ/mol\n",
    "# glucose_in_water = -28.6kJ/mol\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "SQNkV6iF2RBN"
   },
   "source": [
    "The volume reduction for the conversion of 1 mol ideal gas to an (ideal) 1 molar solution under standard conditions (1 atm, 298 K), is considered first. Under given conditions, this value is independent of the type and form of the solute. \n",
    "An expression for this conversion can be derived using the molar Helmholtz energy $A_m$:\n",
    "\n",
    "$$\n",
    "A_m = - RT \\ln(z_i)\n",
    "$$\n",
    "where $z_i = \\gamma^{3/2}V_i$ is a partition function of the system $i$ with volume $V$ and $\\gamma$ being a constant. $R$ is the ideal gas constant and $T$ the thermodynamical temperature.\n",
    "\n",
    "\n",
    "Derive an expression for the volume work (change in Helmholtz energy) for the mentioned conversion from gas to solution and compute its value (in kJ/mol). Define all symbols used in your equations. \n",
    "Implement this standard state correction as a python function with optional arguments for the relevant quantitites."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>\n",
    "\n",
    "Using $z_\\mathrm{solv} = \\gamma^{3/2}V_\\mathrm{solv}$ and $z_\\mathrm{gas} = \\gamma^{3/2}V_\\mathrm{gas}$, we define for the conversion gas $\\rightarrow$ solution:\n",
    "\n",
    "$$\n",
    "\\Delta A_m = A_\\mathrm{solv} - A_\\mathrm{gas} = -RT \\ln(\\frac{z_\\mathrm{solv}}{z_\\mathrm{gas}})\n",
    "$$ \n",
    "which simplifies to\n",
    "\\begin{align*}\n",
    "\\Delta A_m &= -RT \\ln(\\frac{V_\\mathrm{solv}}{V_\\mathrm{gas}})  \\\\\n",
    "&= -RT \\ln(\\frac{p V_\\mathrm{solv}}{nRT}) \\\\ \n",
    "&= -RT \\ln(\\frac{p}{RTc_\\mathrm{solv}}) \\\\\n",
    "\\end{align*}\n",
    "where we have introduced pressure $p$ and concentration $c$. Using the known (standard state) conditions and the concentration of the solution, we get:\n",
    "$$\n",
    "RT\\ln(24.8) \\approx 8~\\mathrm{kJ/mol}\n",
    "$$\n",
    "(Sign changed in last line)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def volume_reduction(T : float = 298, c : float = 1.0, p : float = 1.013e5) -> float:\n",
    "    \"\"\"Compute the volume reduction for the conversion of an ideal gas \n",
    "\n",
    "        Args:\n",
    "            T : thermodynamic temperature (298 K)\n",
    "            c : Concentration of solution (1 mol/L)\n",
    "            p : standard pressure (1.013 * 10^5) Pa\n",
    "            \n",
    "        Returns:\n",
    "            Volume reduction J/mol\n",
    "    \"\"\"\n",
    "    ln_term = np.log(p / (R * T * c * 1e3)) # Conversion for mol/L to mol/m^3\n",
    "    return - R * T * ln_term\n",
    "\n",
    "print(f\"{volume_reduction() / 1000:.2f} kJ/mol\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "rvF86CUG2RBO"
   },
   "source": [
    "Next, we will use equation 2 to compute $\\delta G_{elec}$, as given in the generalized Born model:\n",
    "\n",
    "\\begin{equation}\n",
    "\\delta G_{elec} = \\frac{1}{8\\pi\\epsilon_{0}} \\left( \\frac {1-\\epsilon_{r}}{\\epsilon_{r}} \\right)\n",
    "\\sum_{A}^{N_\\text{atoms,solute}} \\sum_{B}^{N_\\text{atoms,solute}} {\\frac{q_A \\cdot q_B}{f_{GB}(r_{AB}, \\alpha_A, \\alpha_B)}}\n",
    "\\end{equation}\\tag{2}\n",
    "\n",
    "$\\epsilon_{0}$ is the dielectric constant of vacuum and $\\epsilon_{r}$ is the dielectric constant of the solvent (declared above). $q_A$ and $q_B$ are partial charges of atoms $A$ and $B$, respectively. $f_{GB}$ is the empirical screening function used in the GB model, which reads:\n",
    "\\begin{equation}\n",
    "f_{GB}(r_{AB}, \\alpha_A, \\alpha_B) = \\sqrt{ r_{AB}^{2} + \\alpha_{A} \\alpha_{B}  \\cdot exp{\\left( -\\frac {r_{AB}^{2}}{4 \\alpha_{A} \\alpha_{B}} \\right) } }\n",
    "\\end{equation}\\tag{3}\n",
    "\n",
    "$\\alpha_{A} \\cdot \\alpha_{B}$ (each in units of distance, as mentioned above) are empirical parameters. These are chosen to roughly describe the distance of the center of the atom to the solvent surface constituting the molecular cavity.\n",
    "\n",
    "Implement a function to compute the GB screening function and a function to compute $\\delta G_{elec}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fgb(coord : np.array, alpha: np.array) -> float:\n",
    "\n",
    "    \"\"\" Compute screening function from GB model.\n",
    "    \n",
    "    Args:\n",
    "        coordinates (np.array): Cartesian coordinates of atom pair (in Angstrom). Dim: 2 x 3.\n",
    "        alpha (np.array): Vector containing the GB parameters of the atoms (in nm). Dim: 2\n",
    "    \n",
    "    Returns: \n",
    "        Float value of screening function (in meter).\n",
    "    \"\"\"\n",
    "    \n",
    "    assert coord.shape == (2,3), f\"Coordinates don't have correct shape. Expected (2,3), got coordinates.shape\"\n",
    "\n",
    "    rab = np.linalg.norm(coord[0] - coord[1]) \n",
    "\n",
    "    #Handle atom case\n",
    "    if len(alpha) < 2:\n",
    "        #print(\"ATOM DETECTED\")\n",
    "        alpha = np.repeat(alpha,2)\n",
    "\n",
    "        \n",
    "    aab = alpha[0] * alpha[1]\n",
    "\n",
    "    expfct = np.exp(-rab**2/(4*aab))\n",
    "    return np.sqrt(rab ** 2 + aab * expfct) * 1e-10 # Result in meter\n",
    "\n",
    "def calc_gelec(coordinates : np.array, q: np.array, alpha : np.array, eps_r : float = epsilon_h2o) -> float:\n",
    "    \"\"\" Compute electrostatic contribution to solvation free energy using GB model. \n",
    "\n",
    "        Args: \n",
    "            coordinates (np.ndarray): Cartesian coordinates of molecule (in Angstrom). Dim: Nat x 2.\n",
    "            q (np.array): Vector with atomic partial charges of molecule (in e). Dim: Nat.\n",
    "            alpha (np.array): Vector containing atomic GB parameters (in nm).\n",
    "            eps_r (float): Relative permitivity of solute. Defaults to that of water (eps = 78.3).\n",
    "        \n",
    "        Returns:\n",
    "            Gelst (float): Electrostatic contribution to solvation free energy.\n",
    "    \"\"\"\n",
    "    gelec = 0\n",
    "    nat = len(q)\n",
    "    \n",
    "    for a in range(nat):\n",
    "        for b in range(nat): # Iterate over all atom pairs\n",
    "            f =  fgb(np.array([coordinates[a], coordinates[b]]), alpha)\n",
    "            #print(f)\n",
    "            gelec += (q[a] * q[b] * sc.e ** 2) / f\n",
    "\n",
    "    gelec *= 1/(8*np.pi * epsilon0) * ((1 - eps_r) / eps_r)\n",
    "    return gelec * sc.N_A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Everything needed to compute the electrostatic contribution to the solvation free energy in the GB model is present except for atomic partial charges. In the lecture, you have learned about the Mulliken partitioning scheme to compute partial charges $q_A$ of atom $A$ defined as:\n",
    "$$\n",
    "q_A = Z_A - N_{\\mathrm{e},A} = Z_A - \\sum_{\\nu \\in A}^{N_\\mathrm{bf}} \\sum_{nu}^{N_\\mathrm{bf}} D_{\\mu\\nu} S_{\\mu\\nu}\n",
    "$$\n",
    "$Z_A$ are the nuclear charges and $N_{\\mathrm{e},A}$ is the number of electrons \"assigned\" to atom $A$, also known as Mulliken population. \n",
    "\n",
    "\n",
    "Perform Hartree-Fock calculations (STO-3G basis set) to obtain Mulliken charges for $\\mathrm{Li^+}$, $\\mathrm{CO}$ and $\\mathrm{H_2O}$ using the function `mulliken_pop()`. \n",
    "\n",
    "Compare the Mulliken partial charges to those provided in the comment line of the corresponding xyz file of the investigated system and briefly discuss similarities and differences.\n",
    "Use both partial charge schemes to compute $\\delta G_{elec}$ in kJ/mol with water as solvent. Use the provided atomic GB parameters to do so.\n",
    "\n",
    "Give two reasons why Mulliken charges may or may not be feasible for the computation of $\\delta G_{elec}$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "GSASA_li_in_H2O = -0.021784473372\n",
    "GSASA_co_in_H2O = -0.003369403679\n",
    "GSASA_h2o_in_H2O = 0.000822729064"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyscf import gto\n",
    "\n",
    "# Use built-in pyscf function to initialize mol from xyz directly.\n",
    "mol = gto.M(atom=\"data/exercise_5/h2o.xyz\",\n",
    "            basis = \"STO-3G\",\n",
    "            charge = 0,\n",
    "            #verbose=4\n",
    "            )\n",
    "\n",
    "#res = mol.RKS()\n",
    "res = mol.RHF()\n",
    "#res.xc = \"b3lyp\"\n",
    "res.kernel()\n",
    "\n",
    "qa = res.mulliken_pop()[-1]\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#for idx in range(mol.natm):\n",
    "#    print(mol.atom_symbol(idx))\n",
    "#    print(mol.atom_coords()[idx])\n",
    "a0 = sc.physical_constants['Bohr radius'][0]\n",
    "ang2bohr =   1.0e-10 / a0\n",
    "coords = mol.atom_coords() / ang2bohr\n",
    "#print(coords)\n",
    "alphas_ = [gb_param[mol.atom_symbol(idx).lower()] for idx in range(mol.natm)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(qa)\n",
    "dg = calc_gelec(coords, qa, alphas_, eps_r= epsilon_h2o)\n",
    "print(f\" G_elec: {dg / 1000:.2f} kJ/mol\")\n",
    "\n",
    "qa = np.array([-0.172289,0.172289])\n",
    "\n",
    "dg = calc_gelec(coords, qa, alphas_, eps_r= epsilon_h2o)\n",
    "print(f\" G_elec: {dg / 1000:.2f} kJ/mol\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given below are the $G_{SASA}$ contributions for the three systems with water as solvent.\n",
    "\n",
    "Collect all results to determine $\\delta G_{solv}$ according to eq. 1 for the three investigated systems. Briefly comment on the size of each term to $\\delta G_{solv}$ and explain differences among the investigated systems."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Taken from exercise 4 and modified\n",
    "import pandas as pd\n",
    "import scipy.constants as sc\n",
    "\n",
    "def read_in_geometry(file : str) -> tuple[np.array, np.array]:\n",
    "    #mass_dict = {'h': 1.008,'c': 12.011,'n': 14.007,'o': 15.999}\n",
    "\n",
    "    df_ = pd.read_csv(file,skiprows=2,names=[\"atom\",\"x\",\"y\",\"z\"],\n",
    "                      delimiter=r\"\\s+\")\n",
    "    xyz_ = np.column_stack((df_[\"x\"],df_[\"y\"],df_[\"z\"]))\n",
    "    n_atoms = len(df_['atom'])\n",
    "    #masses_ = np.array([mass_dict[atom.lower()] for atom in df_[\"atom\"]]).reshape((n_atoms,1))\n",
    "    return xyz_, np.array([a for a in df_[\"atom\"]])\n",
    "\n",
    "\n",
    "def calc_gsolv(xyzfile : str, qa : np.array, gsasa : float, **kwargs) -> float:\n",
    "    \"\"\" Compute Gsolv based on input geometry and partial charges via GB model following eq. 1.\n",
    "    \n",
    "    Args:\n",
    "        xyzfile (str): Input name of xyzfile.\n",
    "        qa (np.array): Atomic partial charges.\n",
    "        gsasa (float): SASA contribution for specific molecule in specific solvent (provided in the notebook).\n",
    "        \n",
    "    Returns:\n",
    "        gsolv (float): Value of gsolv (in J/mol)    \n",
    "    \"\"\"\n",
    "\n",
    "    # Parse xyzfile \n",
    "    coords, elements = read_in_geometry(xyzfile)\n",
    "    #print(coords)\n",
    "    # First term G_SASA: Should be given as input - Eh\n",
    "    # Define conversion factor Eh -> J\n",
    "    Eh_to_J = (sc.h ** 2) /(4 * np.pi ** 2 * sc.m_e * a0 ** 2)\n",
    "    gsasa *= Eh_to_J * sc.N_A\n",
    "\n",
    "    \n",
    "    # Second term G_elec - J/mol\n",
    "    alphas_ = [gb_param[a.lower()] for a in elements]\n",
    "    gelec = calc_gelec(coords, qa, alphas_, **kwargs)\n",
    "\n",
    "    # Third term Wvol - J/mol\n",
    "    wvol = volume_reduction()\n",
    "\n",
    "    gsolv = gsasa + gelec + wvol\n",
    "\n",
    "    return gsolv"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# All values given in Hartree\n",
    "GSASA_li_in_H2O = -0.021784473372\n",
    "GSASA_co_in_H2O = -0.003369403679\n",
    "GSASA_h2o_in_H2O = 0.000822729064"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "MkCFPBkM2RBQ",
    "outputId": "f9064c61-786c-4592-a672-1a10ca90e56a"
   },
   "outputs": [],
   "source": [
    "# Li as example\n",
    "q_ = np.array([1])\n",
    "calc_gsolv(\"data/exercise_5/li.xyz\", q_, GSASA_li_in_H2O)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Mdk4jVkBLCw3"
   },
   "source": [
    "Compute $\\delta G_{solv}$ for glucose as well using your function definitions up to this point. Here it is sufficient to only use the set of reference charges (provided below but not in the corresponding xyz file)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "q_glucose = np.array([ 0.050598, 0.058670, 0.023631, 0.041975, -0.237740, 0.039032, 0.123486, 0.026054,-0.259155,-0.202893, 0.024048,-0.227796, 0.053967, 0.025953, 0.029978,-0.244731,-0.252901, 0.017935, 0.180045, 0.027882, 0.178165, 0.183247, 0.154304, 0.186233]\n",
    "                     )\n",
    "GSASA_glucose_in_H2O = -0.001474618146 #Hartree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "G8FvNuIoLCw4"
   },
   "outputs": [],
   "source": [
    "calc_gsolv(\"data/exercise_5/glucose.xyz\", q_glucose, GSASA_glucose_in_H2O)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "BjzWZnkeLCw4"
   },
   "source": [
    "### Task 2\n",
    "\n",
    "So far, we have calculated the free energy of solvation for some molecules in water, but in principle this can be extended to any solute and solvent we are interested in. In this task, we will look at other solutes (Toluene, THF and Acetonitrile).\n",
    "The dielectric constants for these solvents are provided below. A series of files containing partial charges from an external calculation are provided alongside the xyz files of the molecules. The file `glucose_tol.chg` contains $N_\\mathrm{atm}$ partial charges of the glucose molecule where toluene has been used to emulate the implicit solvent. The same ordering of element symbols as in the xyz file is used to write-out these charges. In the last row, the corresponding SASA contribution (again in Hartree) is given, highlighted with \"SASA:\".\n",
    "\n",
    "Write a reader function to work with the provided partial charge and SASA information.\n",
    "\n",
    "Use the given data to compute the solvation free energies of the corresponding solutes and solvents for all four systems. Briefly discuss differences for a) different partial charges (and solutes) and b) neutral vs. charged species. \n",
    "\n",
    "Compare your values against the ones generated for water as a solvent in Task 1 and explain differences."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "wYDaGhKhLCw5"
   },
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reader function for .chg file\n",
    "def read_chg(fileinp : str) -> tuple[np.array,float]:\n",
    "    \"\"\" Read atomic partial charges and SASA contribution from file.\n",
    "    \n",
    "    Args:\n",
    "        fileinp (str): Input chg file.\n",
    "    \n",
    "    Returns:\n",
    "        qa (np.array): Vector of partial charges of corresponding system.\n",
    "        sasa (float): Gsasa contribution.\n",
    "    \"\"\"\n",
    "\n",
    "    charges = []\n",
    "    sasa = None\n",
    "\n",
    "    with open (fileinp) as f:\n",
    "        lines = f.readlines()\n",
    "    \n",
    "    for line in lines:\n",
    "        #line = line.strip()  # Remove extra whitespace and newline characters\n",
    "        if line.startswith(\"SASA:\"):\n",
    "            # Extract SASA value\n",
    "            try:\n",
    "                sasa = float(line.split(\":\")[1].strip())\n",
    "            except ValueError:\n",
    "                raise ValueError(f\"Invalid SASA value in line: {line}\")\n",
    "        else:\n",
    "            # Try to parse the line as a charge\n",
    "            try:\n",
    "                charges.append(float(line))\n",
    "            except ValueError:\n",
    "                raise ValueError(f\"Invalid charge value in line: {line}\")\n",
    "    \n",
    "    if sasa is None:\n",
    "        raise ValueError(\"SASA value not found in the file.\")\n",
    "    \n",
    "    return np.array(charges), sasa"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qs, sasa = read_chg(\"data/exercise_5/li_tol.chg\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Zpa1q0dALCw6"
   },
   "source": [
    "### Task 3\n",
    "\n",
    "As a final task, calculate the free energy of solvation for $\\mathrm{Na^+}$ and $\\mathrm{Cl^-}$ with all solvents used before.\n",
    "\n",
    "The free energy of solvation for ions appears to be on another magnitude than for the non-charged molecules, most notably for polar solvents. Explain this observation based on the physical model used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "2LbumUA5LCw6"
   },
   "outputs": [],
   "source": [
    "GSASA_Na_in_acetonitrile = -0.01542349163 \n",
    "GSASA_Na_in_water = -0.012275538580 \n",
    "GSASA_Na_in_toluene = -0.006477033412 \n",
    "GSASA_Na_in_THF = -0.014041240751 \n",
    "\n",
    "GSASA_Cl_in_acetonitrile = -0.004539782458 \n",
    "GSASA_Cl_in_water = -0.000221933034 \n",
    "GSASA_Cl_in_toluene = -0.002560078983 \n",
    "GSASA_Cl_in_THF = -0.005751831357 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Generate sodium input file\n",
    "\"\"\"\n",
    "1\n",
    "\n",
    "Na 0 0  0\n",
    "\"\"\"\n",
    "qna = np.array([1])\n",
    "\n",
    "gs_na_tol = calc_gsolv(\"data/exercise_5/na.xyz\", qna, GSASA_Na_in_toluene, eps_r = epsilon_tol)\n",
    "print(f\"Gsolv Na+ in Tol: {gs_na_tol/1000:.2f} kJ/mol\")\n",
    "\n",
    "gs_na_thf = calc_gsolv(\"data/exercise_5/na.xyz\", qna, GSASA_Na_in_THF, eps_r = epslion_thf)\n",
    "print(f\"Gsolv Na+ in THF: {gs_na_thf/1000:.2f} kJ/mol\")\n",
    "\n",
    "gs_na_water = calc_gsolv(\"data/exercise_5/na.xyz\", qna, GSASA_Na_in_water)\n",
    "print(f\"Gsolv Na+ in H2O: {gs_na_water/1000:.2f} kJ/mol\")\n",
    "\n",
    "gs_na_mecn = calc_gsolv(\"data/exercise_5/na.xyz\", qna, GSASA_Na_in_acetonitrile,eps_r = epsilon_mecn)\n",
    "print(f\"Gsolv Na+ in MeCN: {gs_na_mecn/1000:.2f} kJ/mol\")\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "exvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
