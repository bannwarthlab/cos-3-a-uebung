{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "SNefvaXhunVq"
   },
   "source": [
    "# COS 3a Exercise 10\n",
    "\n",
    "---\n",
    "Submission until 07/01/2024 12:00 p.m."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "VzPNeZ8lunVs"
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Python Tutorial\n",
    "\n",
    "In the course of this exercise, we need to store complex values. Complex values are numbers with a real and an imaginary part like: \n",
    "\n",
    " $z=3 + 2i$\n",
    "\n",
    "Whereas $i$ is the imaginary unit which satisfies $i^2=-1$. \n",
    "\n",
    "In Python, we can store complex values using different methods. The simplest way is to use the literal expression `3+2j`. \n",
    "\n",
    "Note that the imaginary unit in Python is denoted by `j` instead of `i`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = 3 + 2j\n",
    "print('real:',z.real)\n",
    "print('imaginary:',z.imag)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another possibility is to use the `complex()` function. The `complex()` function returns a complex number when real and imaginary parts are provided as arguments. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = complex(3,2)\n",
    "print('real:',z.real)\n",
    "print('imaginary:',z.imag)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To specify a complex data array in Python, we can use `numpy`. \n",
    "\n",
    "On the one hand, we can manually specify the `dtype` of the array as `complex`. This is especially useful when intializing data arrays that are supposed to store complex values, but do not contain imaginary parts yet.\n",
    "\n",
    "On the other hand, `numpy` automatically infers the `dtype` of the array to `complex` when at least one of the provided values contains an imaginary part."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# no complex element is given, but the dtype is complex\n",
    "a = np.array([1, 2, 3], dtype=complex)\n",
    "print('No complex value given (dtype=complex):',a)\n",
    "\n",
    "# Only the first element is complex, but numpy will convert the other elements aswell\n",
    "b = np.array([1+0j, 2, 3])\n",
    "print('One element is complex:', b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "8tCYycseunVt"
   },
   "source": [
    "# Introduction\n",
    "\n",
    "## Fourier Transform (FT)\n",
    "\n",
    "The Fourier transform is an integral transform, it takes a function in one domain as an input and returns function in another domain as output.\n",
    "\n",
    "The output function is complex-valued and describes the extent to which different frequencies are present in the original function.\n",
    "\n",
    "The general equation for the Fourier transform is given as:\n",
    "\n",
    "\\begin{equation*}\n",
    "F(k) = \\int_{-\\infty}^{\\infty} f(x) e^{-i2 \\pi  k x}dx\n",
    "\\end{equation*}\n",
    "\n",
    "Where $x$ is the independent variable of the original function, $f(x)$ is the dependent variable of the original function, $k$ is the independent variable of the transformed function and $F(k)$ is the dependent variable of the transformed function.\n",
    "\n",
    "https://en.wikipedia.org/wiki/Fourier_transform\n",
    "\n",
    "## Discrete Fourier Transform (dFT)\n",
    "\n",
    "The discrete Fourier transform (dFT) is a mathematical operation that converts a finite sequence of equidistant samples of a function into a sequence of the same length of equidistant samples of the discrete-time Fourier transform (DTFT), which is a complex-valued frequency function.\n",
    "\n",
    "The dFT makes it possible to analyze the frequency components of a signal, such as amplitude, phase and energy. The dFT can be calculated efficiently with the fast Fourier transform (FFT) algorithm.\n",
    "\n",
    "The expresion for the Discrete Fourier Transform is:\n",
    "\n",
    "\\begin{equation*}\n",
    "X_k = \\sum_{n = 0}^{N-1} x_n e^{-i2 \\pi n  k/N }dx\n",
    "\\end{equation*}\n",
    "\n",
    "Where $x_n$ is the value of the original sequence at time index $n$. $k$ is the frequency index of the transformed sequence, $X_k$ is the value of the transformed sequence at the frequency index $k$ and $N$ is the length of the sequence.\n",
    "\n",
    "That means we need to collect a signal of $N$ data points with values $x$. \n",
    "\n",
    "https://en.wikipedia.org/wiki/Discrete_Fourier_transform\n",
    "\n",
    "## Fast Fourier Transform (FFT)\n",
    "\n",
    "In order to compute the Discrete Fourier Transform (dFT) of an equidistant sample, we can use the FFT algorithm which is an efficient way to compute the dFT.\n",
    "\n",
    "https://en.wikipedia.org/wiki/Fast_Fourier_transform"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "b1lFfhNYunVu"
   },
   "source": [
    "The following example shows a continous signal, which is represented by a $sin$ function (<span style=\"color:red\">red</span>). Furthermore, the signal is sampled at $N$ equidistant points (<span style=\"color:blue\">blue</span>). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Sinus function\n",
    "def f_(x: np.array, freq: np.array) -> np.array:\n",
    "    \"\"\"\n",
    "    Args:\n",
    "        x (np.array): The input value\n",
    "        freq (np.array): The frequency\n",
    "    Returns:\n",
    "        np.array : The sine of the input value multiplied by the frequency.\n",
    "    \"\"\"\n",
    "    w = 2*np.pi*freq\n",
    "    return np.sin(w*x)\n",
    "\n",
    "# Sampling rate (sr) and sampling interval (si)\n",
    "sr = 100\n",
    "si = 0.1 # So a sample every 0.1 seconds\n",
    "\n",
    "x = np.linspace(0, 1, sr)\n",
    "freq = 1.\n",
    "\n",
    "# Continuous plot\n",
    "plt.figure(figsize = (10, 6))\n",
    "plt.plot(x, f_(x, freq=freq), label='f(x)', color='red')\n",
    "\n",
    "# Sampled points\n",
    "x_ = np.arange(0, 1, si)\n",
    "sampled = f_(x_, freq=freq)\n",
    "plt.plot(x_, sampled,'bo', label='sampled')\n",
    "\n",
    "plt.xlabel('t [s]')\n",
    "plt.ylabel('Amplitude')\n",
    "plt.legend()\n",
    "plt.show()\n",
    "\n",
    "print('Sampled points:', sampled.round(2))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "cUEDigz_unVv"
   },
   "source": [
    "Instead of a continuous signal over $t$, we now have discrete data points at evenly spaced intervals in time $t_n = \\frac{n}{N}$ with $n \\in \\mathbb{N} \\cap [0, N)$.\n",
    "\n",
    "In the discrete Fourier transform (dFT), we transform the vector $\\mathbf{x}$ (our discrete data points) to a corresponding vector in Fourier space $\\mathbf{X}$, which contains the values $X_k$.\n",
    "\n",
    "The transformation matrix is determined by evaluating $e^{-i2\\pi kn/N}$ for the respective values of $n$ and $k$.\n",
    "\n",
    "We use the notation $w = e^{-i2\\pi /N}$ and can write the transformation as:\n",
    "\n",
    "\\begin{equation*}\n",
    "  \\begin{pmatrix}\n",
    "  X_0 \\\\\n",
    "  X_1 \\\\\n",
    "  X_2 \\\\\n",
    "  \\vdots \\\\\n",
    "  X_{N-1}\n",
    "  \\end{pmatrix}\n",
    "  =\n",
    "  \\begin{pmatrix}\n",
    "  1 & 1 & 1 & \\cdots & 1 \\\\\n",
    "  1 & w & w^2 & \\cdots & w^{N-1} \\\\\n",
    "  1 & w^2 & w^4 & \\cdots & w^{2(N-1)} \\\\\n",
    "  \\vdots & \\vdots & \\vdots & \\ddots & \\vdots \\\\\n",
    "  1 & w^{N-1} & w^{2(N-1)} & \\cdots & w^{(N-1)(N-1)}\n",
    "  \\end{pmatrix}\n",
    "  \\begin{pmatrix}\n",
    "  x_0 \\\\\n",
    "  x_1 \\\\\n",
    "  x_2 \\\\\n",
    "  \\vdots \\\\\n",
    "  x_{N-1}\n",
    "  \\end{pmatrix}\n",
    "\\end{equation*}\n",
    "\n",
    "The first vector represents the transformed function at discrete points in the frequency domain. The matrix is the transformation matrix with dimensions $N \\times N$. The last vector represents the function at discrete points in the time domain.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "s5BDPOI6unVw"
   },
   "source": [
    "This equation can be rewritten as a linear algebra problem, where the values of $\\mathbf{X}$ are complex numbers. Formally, we need to perform a matrix-vector multiplication, i.e., $N \\times N$ element-wise multiplications to obtain $\\mathbf{X}$, resulting in an overall quadratic scaling.\n",
    "\n",
    "For the example shown above, $\\mathbf{X}$ will contain only zeros, except for $X_1 = -X_9 = -i \\cdot 0.5$.\n",
    "\n",
    "### Information on the FFT\n",
    "\n",
    "The Fast Fourier Transform (FFT) is an efficient algorithm for calculating the discrete Fourier Transform (dFT) of a sequence. This algorithm was first described in Cooley and Tukey’s paper from 1965. Interestingly, Gauss was aware of this algorithm more than 150 years earlier, as indicated in his unpublished work from 1805.\n",
    "\n",
    "The FFT algorithm recursively breaks the dFT into smaller dFTs, reducing the computational scaling from $O(N^2)$ to $O(N \\cdot \\log_2 N)$, where $N$ is the number of data points. This reduction in computation time distinguishes FFT from the regular dFT.\n",
    "\n",
    "FFT is widely used in engineering and science, particularly in signal processing and data compression. In the regular dFT, one can set up this matrix and multiply it with the samples in the time (or real space) domain.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "yA3CxIUqunVw"
   },
   "source": [
    "## Task 1\n",
    "\n",
    "In the first task we will proof that the Fourier transform is periodic. Then we will generate our input data from a user-defined function.\n",
    "\n",
    "To proof that $X_k = X_{k+N}$ we need to use $e^{−𝑖2\\pi 𝑚} = 1, ∀ 𝑚 ∈ N$ and the expression for the discrete FT.\n",
    "\n",
    "#### 1.1 Show that $X_{k+N} = X_{k}$, where $N$ is the total number of data points.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "Start with the definition of the dFT:\n",
    "\\begin{equation*}\n",
    "X_k = \\sum_{n=0}^{N-1} x_n e^{-i 2 \\pi k n / N}\n",
    "\\end{equation*}\n",
    "\n",
    "Consider $ X_{k+N} $:\n",
    "\\begin{equation*}\n",
    "X_{k+N} = \\sum_{n=0}^{N-1} x_n e^{-i 2 \\pi (k+N) n / N}\n",
    "\\end{equation*}\n",
    "\n",
    "Expand the exponent:\n",
    "\\begin{equation*}\n",
    "X_{k+N} = \\sum_{n=0}^{N-1} x_n e^{-i 2 \\pi k n / N} e^{-i 2 \\pi N n / N}\n",
    "\\end{equation*}\n",
    "\n",
    "Simplify the second exponential term:\n",
    "\\begin{equation*}\n",
    "e^{-i 2 \\pi N n / N} = e^{-i 2 \\pi n}\n",
    "\\end{equation*}\n",
    "\n",
    "Since $ e^{-i 2 \\pi n} = 1 $ for any integer $ n $, this reduces to:\n",
    "\\begin{equation*}\n",
    "X_{k+N} = \\sum_{n=0}^{N-1} x_n e^{-i 2 \\pi k n / N} \\cdot 1\n",
    "\\end{equation*}\n",
    "\n",
    "Thus:\n",
    "\\begin{equation*}\n",
    "X_{k+N} = \\sum_{n=0}^{N-1} x_n e^{-i 2 \\pi k n / N} = X_k\n",
    "\\end{equation*}\n",
    "\n",
    "---\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### 1.2 Generate a superposition of three cosine functions and plot the superposition:\n",
    "\n",
    "\\begin{equation*}\n",
    "𝑓(𝑡)= A_a ⋅cos\\omega_a t + A_b ⋅cos\\omega_b 𝑡 + 𝐴_c ⋅cos\\omega_c t\n",
    "\\end{equation*}\n",
    "\n",
    "with\n",
    "\n",
    "\\begin{equation*}\n",
    "A_a = 1.0,\\, 𝐴_b = 3.0,\\, A_c = 5.0\n",
    "\\end{equation*}\n",
    "\n",
    "and\n",
    "\n",
    "\\begin{equation*}\n",
    "\\omega_a = 2\\pi⋅100 \\,\\mathrm{Hz}  ,\\,\\omega_b  =2\\pi⋅25 \\,\\mathrm{Hz}  ,\\,\\omega_c  =2\\pi ⋅ 1 \\,\\mathrm{Hz} \n",
    "\\end{equation*}\n",
    "\n",
    "We will sample $\\bf{x}$ with $N$ = 512 from 𝑓(𝑡) on a time interval between 0 and 1 s."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 522
    },
    "id": "axHcU9vjunVx",
    "outputId": "9b7268b2-abc5-4271-bebd-6e842085c79b"
   },
   "outputs": [],
   "source": [
    "# Variables\n",
    "sr = 512 # sample rate\n",
    "time=1.0 # total time in seconds\n",
    "ts = time/sr # time step\n",
    "\n",
    "# write a vector with evenly spaced points in time from 0 to t\n",
    "t = np.arange(0,time,ts)\n",
    "\n",
    "# Define here the function to be plotted\n",
    "# You can play around with it and see how the plot changes if, for example you add more A and w values. \n",
    "# !! A and w must have the same length !!\n",
    "def f_(A : np.array, w: np.array, t: np.array) -> np.array:\n",
    "    \"\"\"\n",
    "    This function will result in a sum of cosines with different amplitudes and frequencies.\n",
    "    Args:\n",
    "        A (np.array): The amplitudes.\n",
    "        w (np.array): The frequencies.\n",
    "        t (np.array): The time.\n",
    "    Returns:\n",
    "        nparray : The sum of the amplitudes multiplied by the frequencies.\n",
    "    \"\"\"\n",
    "    res = np.zeros_like(t)\n",
    "    for ind, value in enumerate(A):\n",
    "        res += value*np.cos(w[ind]*t)\n",
    "    \n",
    "    return res\n",
    "\n",
    "# Define here all the parts (A1, w1, A2, ...) of the function given above and calculate their value for every value of t\n",
    "A = [1., 3., 5.]\n",
    "w = [100.0*2.0*np.pi/time, 25.0*2.0*np.pi/time, 1.0*2.0*np.pi/time]\n",
    "\n",
    "x = f_(A, w, t)\n",
    "# plot the function\n",
    "plt.figure(figsize = (10, 6))\n",
    "\n",
    "plt.plot(t, x, 'r')\n",
    "\n",
    "plt.xlabel('t [s]')\n",
    "plt.ylabel('Amplitude')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "P9IWd3dJunVy"
   },
   "source": [
    "## Task 2\n",
    "\n",
    "#### 2.1 Define a function to compute the dFT and then plot the absolute Fourier coefficients of the function defined in Task 1.\n",
    "\n",
    "Refere to the introduction for a detailed explanation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "xJjZaIKsunVy"
   },
   "outputs": [],
   "source": [
    "# dFT Function\n",
    "def omega(ii : int, jj : int, N : int) -> complex:\n",
    "    return np.exp( -2j*np.pi*ii*jj/N )\n",
    "\n",
    "def dFT(x : np.array) -> np.array:\n",
    "    \"\"\"\n",
    "    This function will calculate the discrete Fourier transformation of a given signal.\n",
    "    Args:\n",
    "        x (np.array): The input signal.\n",
    "    Returns:\n",
    "        np.array: The Fourier transformation of the input signal.\n",
    "    \"\"\"\n",
    "    # We allocate the memory for the result. This is a vector of complex numbers with dimension N. \n",
    "    # Note: Memory allocation is generally not needed in python, but it is good practice to allocate memory before hand, to ensure simplicity.\n",
    "    xf = np.zeros_like(x, dtype=complex) \n",
    "    N = x.size\n",
    "    for n in range(0,N):\n",
    "        for m in range(0,N):\n",
    "            xf[n] += omega(n, m, N) * x[m]\n",
    "            \n",
    "    return xf\n",
    "\n",
    "def dFT_la(x : np.array) -> np.array:\n",
    "    \"\"\"\n",
    "    This function will calculate the discrete Fourier transformation of a given signal.\n",
    "    Args:\n",
    "        x (np.array): The input signal.\n",
    "    Returns:\n",
    "        np.array: The Fourier transformation of the input signal.\n",
    "    \"\"\"\n",
    "    xf = np.zeros_like(x, dtype=complex) \n",
    "    N = x.size\n",
    "    #alternative formulation using linear algebra\n",
    "    #first we create the Matrix M using the omega function\n",
    "    M = np.fromfunction(lambda i, j : omega(i,j, N=N), shape=(N,N))\n",
    "    # then we do a matrix vector multiply, to get our final xf values\n",
    "    xf = M @ x\n",
    "    return xf\n",
    " "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 542
    },
    "id": "PdrwdL8JunVy",
    "outputId": "3d256935-ebfd-4d8c-db36-d2112c3cfbef"
   },
   "outputs": [],
   "source": [
    "# Call the dFT and plot the norm of the transformed data\n",
    "xf = dFT(x)\n",
    "amp = np.abs(xf)\n",
    "\n",
    "# plot in the frequency domain\n",
    "plt.figure(figsize = (10, 6))\n",
    "k = np.arange(0,sr,1)\n",
    "plt.stem(k, amp, linefmt='r-', markerfmt=' ')\n",
    "plt.ylabel('|X|/N',style='italic')\n",
    "plt.xlabel('k',style='italic')\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "CVfPvdBHunVz"
   },
   "source": [
    "\n",
    "\n",
    "#### 2.2 Print out the number of non-zero points in the Fourier transformed data. For simplicity, we consider every element larger than 0.001 to be nonzero. Also give the ratio of the non-zero points relative to the full data set."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/"
    },
    "id": "lqYCStP1unVz",
    "outputId": "a06b88ff-7a05-4c68-ef26-aa35a8456bb6"
   },
   "outputs": [],
   "source": [
    "nonzero = np.count_nonzero(amp > 0.001)\n",
    "\n",
    "print('Number of non-zero values:', nonzero)\n",
    "print('non-zero/total:', nonzero/len(amp))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gsQ6RwyHunVz"
   },
   "source": [
    "#### 2.3 Define an inverse discrete FT function and plot the inverse of the fourier transformation.\n",
    "\n",
    "Note: The inverse function should be able to reconstruct the plot in Task 1.b by applying the inverse FT to the transformed data from Task 2.a."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 522
    },
    "id": "ZnJNfo5YunVz",
    "outputId": "9fb2267d-eb3c-432e-d12e-b0ffe16f100a"
   },
   "outputs": [],
   "source": [
    "# simple inverse discrete FT\n",
    "def inv_dFT(x : np.array) -> np.array:\n",
    "    \"\"\"\n",
    "    This function will calculate the inverse discrete Fourier transformation of a given signal.\n",
    "    Args:\n",
    "        x (np.array): The input signal (frequency).\n",
    "    Returns:\n",
    "        np.array: The inverse Fourier transformation of the input signal.\n",
    "    \"\"\"\n",
    "    \n",
    "    xff = dFT_la(x)\n",
    "\n",
    "    return xff/x.size\n",
    "\n",
    "xff = inv_dFT(xf)\n",
    "\n",
    "# plot the original function and the backtransformed function\n",
    "plt.figure(figsize = (10, 6))\n",
    "plt.plot(t, xff, 'r', label='Backtransformed')\n",
    "plt.plot(t, x, 'b', label='Original', linestyle='dashed', dashes=(3, 5))\n",
    "plt.ylabel('Amplitude')\n",
    "plt.xlabel('t [s]')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "fUkCAhQ-unV0"
   },
   "source": [
    "\n",
    "#### 2.4 (OPTIONAL) Define a recursive FFT function based on the algorithm proposed by Cooley-Tukey. Call your FFT function and calculate the fourier transformation of the function in Task 1. Plot your results.\n",
    "\n",
    "Note: The input signal to FFT should have a length of power of 2. If this is not given you can pad the input signal with zeros.\n",
    "\n",
    "\\begin{equation}\n",
    "X_k=\\sum_{n=0}^{N / 2-1} x_{2 n} \\cdot e^{-i 2 \\pi k n /(N / 2)}+e^{-i 2 \\pi k / N} \\sum_{n=0}^{N / 2-1} x_{2 n+1} \\cdot e^{-i 2 \\pi k n /(N / 2)}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "WXfnPvFlunV0"
   },
   "outputs": [],
   "source": [
    "# Define your own recursive FFT(x) function based on Cooley-Tukey\n",
    "def FFT(x : np.array) -> np.array:\n",
    "    \"\"\"\n",
    "    Compute the Fast Fourier Transform (FFT) of an input sequence.\n",
    "\n",
    "    Parameters:\n",
    "    x (list or numpy array): Input sequence to transform. The length of the sequence must be a power of 2.\n",
    "\n",
    "    Returns:\n",
    "    numpy array: The FFT of the input sequence, represented as a numpy array of complex numbers.\n",
    "\n",
    "    Notes:\n",
    "    This implementation uses the Cooley-Tukey FFT algorithm, which recursively divides the input sequence\n",
    "    into even and odd indexed elements and combines their FFTs. The time complexity of this algorithm is O(N log N).\n",
    "\n",
    "    \"\"\"\n",
    "    N = len(x)\n",
    "    if N == 1:\n",
    "        return x\n",
    "    else:\n",
    "        X = np.zeros(N, dtype=complex)\n",
    "        Xeven=FFT(x[0::2])\n",
    "        Xodd=FFT(x[1::2])\n",
    "        for m in range(0,N):\n",
    "            m_split=m % (N//2)\n",
    "            X[m]=Xeven[m_split]+ np.exp(-2j * np.pi * m / N) * Xodd[m_split]\n",
    "        return X"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 614
    },
    "id": "yHgUqKvvunV0",
    "outputId": "0171ce1a-f096-4c06-d371-537b618eca43"
   },
   "outputs": [],
   "source": [
    "# Call the FFT and plot the norm of the transformed data\n",
    "xf=FFT(x)\n",
    "amp=np.abs(xf)/len(xf)\n",
    "# plot in the frequency domain\n",
    "plt.figure(figsize = (10, 6))\n",
    "f = np.arange(0,sr,1)\n",
    "plt.stem(f, amp, linefmt='r-', markerfmt=' ')\n",
    "plt.ylabel('|X|/N',style='italic')\n",
    "plt.xlabel('k',style='italic')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "086wGj4runV0"
   },
   "source": [
    "## Task 3: Image Compression\n",
    "The fast Fourier transform (FFT) is a powerful tool for image compression, allowing us to efficiently represent the frequency content of an image. Image compression reduces the size of an image file by eliminating redundant or unnecessary information. This process leverages the fact that the human eye is less sensitive to high-frequency details, such as fine textures.\n",
    "\n",
    "One effective approach to image compression involves transforming the image into the frequency domain using the 2D-FFT, then removing small amplitude components. This method smooths out fine details and textures, reducing the amount of information that needs to be stored. The simplified image can then be transformed back into the spatial domain using the inverse 2D-FFT, resulting in a lower resolution version of the original image.\n",
    "\n",
    "This technique, known as lossy compression, permanently removes some information from the original image. It is suitable for images that do not require high detail, such as web graphics or images viewed from a distance. For images needing high detail, such as medical images or photographs, lossless compression methods are preferred, as they preserve all original information.\n",
    "\n",
    "Using `numpy`'s built-in `fft2` (2D-FFT) and `ifft2` (inverse 2D-FFT) functions, we can recreate an image. Below is a template for performing 2D-FFT and inverse 2D-FFT, inspired by Steven L. Brunton and J. Nathan Kutz's work (Chapter 2): http://databookuw.com/. Note that numpy's 1D-FFT routines, similar to those you coded above, are called `fft` and `ifft`.\n",
    "\n",
    "\n",
    "#### 3.1 Using this template, try to compress the image available for download from Moodle or the Jupyter hub. We will work with the grayscale component, using the matrix $\\bf{Abw}$ obtained by converting the colored image to grayscale."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "colab": {
     "base_uri": "https://localhost:8080/",
     "height": 391
    },
    "id": "flX-yrdZunV1",
    "outputId": "0b9e7e0b-d89f-4b0f-f619-b9d5f7202005"
   },
   "outputs": [],
   "source": [
    "from matplotlib.image import imread\n",
    "plt.rcParams['figure.figsize'] = [12, 8]\n",
    "plt.rcParams.update({'font.size': 18})\n",
    "\n",
    "#importing an image\n",
    "A = imread('../data/exercise_10/birds.jpg')\n",
    "Abw = np.mean(A, -1); # Convert RGB to grayscale\n",
    "\n",
    "# plot image via matplotlib\n",
    "plt.imshow(Abw,cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()\n",
    "\n",
    "# Compute 2D-FFT of image using fft2 and store in At\n",
    "At = np.fft.fft2(Abw)\n",
    "\n",
    "# Put the FFT on a log scale and plot\n",
    "F = np.log(np.abs(np.fft.fftshift(At))+1) # Put FFT on log scale\n",
    "plt.imshow(F,cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()\n",
    "\n",
    "\n",
    "# Compute Inverse FFT, recreate the image back and plot it\n",
    "Ar = np.real(np.fft.ifft2(At)).astype('uint8')\n",
    "plt.imshow(Ar,cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "o1rFynHhunV1"
   },
   "source": [
    "#### 3.2 Using the 2D-FFT of the image from 3.1, zero-out all components that are smaller than the given threshold and recompute the image from the pruned data."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<span style=\"color:red\"> Solution: </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The threshold defines, which amplitudes in the FFT are considered to be noise and can be neglected.\n",
    "thresh = 10000 # play around with this value and see how the image changes\n",
    "\n",
    "print(\"Every amplitude below %f will be neglected\" % thresh)\n",
    "index=np.zeros_like(At,dtype=float)\n",
    "index = (np.abs(At) > thresh).astype(float)  # 0 if false, 1 if true\n",
    "\n",
    "# All data with below the threshold shall be zeroed out:\n",
    "Atlow = At * index\n",
    "\n",
    "# Put the pruned FFT on a log scale and plot\n",
    "Flow = np.log(np.abs(np.fft.fftshift(Atlow))+1) # Put FFT on log scale\n",
    "plt.imshow(Flow,cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()\n",
    "\n",
    "# Now use the inverse 2D-FFT to transform back an plot the resulting image from the pruned data\n",
    "Arlow = np.real(np.fft.ifft2(Atlow)).astype('uint8')\n",
    "plt.imshow(Arlow,cmap='gray')\n",
    "plt.axis('off')\n",
    "plt.show()\n",
    "\n",
    "# we now know, which elements in the transformed data At were below the threshold and could be zeroed out.\n",
    "# Print out the percentage of the kept data here.\n",
    "print(\"Hence, we keep %.2f %% of the %d datapoints\" % (np.mean(index)*100.0,index.size))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Example for multiple threshold values\n",
    "thresh = np.linspace(0, 500000, 6).astype(int)\n",
    "\n",
    "# Create a figure with 10 subplots\n",
    "fig, axs = plt.subplots(2, 3, figsize=(20, 10))\n",
    "axs = axs.ravel()\n",
    "\n",
    "for i, t in enumerate(thresh):\n",
    "    index = (np.abs(At) > t).astype(float)\n",
    "    Atlow = At * index\n",
    "    Arlow = np.real(np.fft.ifft2(Atlow)).astype('uint8')\n",
    "    axs[i].imshow(Arlow, cmap='gray')\n",
    "    axs[i].axis('off')\n",
    "    axs[i].set_title('Threshold: %d' % t)\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "3.8",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
