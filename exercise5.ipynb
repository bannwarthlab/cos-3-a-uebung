{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 5\n",
    "\n",
    "---\n",
    "Submission until 19/11/2024 12:00 p.m.\n",
    "\n",
    "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Solvation free energy\n",
    "\n",
    "The goal of this exercise will be to improve our understanding of the continuum solvation models, in particular,\n",
    "the (generalized) Born model.\n",
    "We will calculate the free energy of solvation for different molecules in different solvents using the generalized Born solvation model.\n",
    "Furthermore, we will compute the vaporization enthalpies for water and\n",
    "we will compare the obtained result with reference values from experiments.\n",
    "\n",
    "### Continuum solvation models\n",
    "\n",
    "A continuum solvation model considers the solvent as a uniform polarizable medium with a specific dielectric constant.\n",
    "Hence, the solute is placed in an appropriately shaped cavity in this medium.\n",
    "\n",
    "The free energy of solvation can be divided into 4 different contributions:\n",
    "\n",
    "$$\n",
    "\\delta G_{solv} = \\delta G_{cavity} + \\delta G_{elec} + \\delta G_{vdW} + W_{vol}\n",
    "$$\n",
    "\n",
    "$\\delta G_{cavity}$ is the free energy cost, required to \"dig\" a cavity in the solvent in which the solute remains.\n",
    "\n",
    "$\\delta G_{elec}$ is the term that refers to the change in the electrostatic interactions of the solute due to solvation.\n",
    "This includes interactions with the solvent molecules, but also changed interactions within the solute.\n",
    "In a continuum model, the inner cavity is filled by the solute which is assumed to have a\n",
    "relative permittivity $\\varepsilon_r=1$, while it is a different solvent-specific value for the solvent medium.\n",
    "\n",
    "$\\delta G_{vdW}$ is the third term that describes all changed noncovalent interactions (Pauli repulsion and London dispersion) that are not included in $\\delta G_{elec}$.  \n",
    "\n",
    "The last term $W_{vol}$, corresponds to the volume reduction during the change of the standard state.\n",
    "For every species, this is a constant contribution and affects relative free energies for processes with changing number\n",
    "of particles.\n",
    "\n",
    "Many models consider $\\delta G_{cavity}$ and $\\delta G_{vdW}$ in one simplified term, which is obtained by the solvent accessible surface area (SASA): \n",
    "\n",
    "$$\n",
    "\\delta G_{SASA} ≈ \\delta G_{cavity} + \\delta G_{vdW} \n",
    "$$\n",
    "\n",
    "giving: \n",
    "\n",
    "\\begin{equation}\n",
    "\\delta G_{solv} ≈ \\delta G_{SASA} + \\delta G_{elec} + W_{vol} \n",
    "\\end{equation}\\tag{1}\n",
    "\n",
    "\n",
    "$\\delta G_{SASA}$ contains empirical terms, and these will be provided for every free energy calculation in this exercise. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "57vzlqbm2RBL"
   },
   "source": [
    "### Task 1\n",
    "\n",
    "In this task, we will calculate the free energy of solvation for different ions, namely $\\mathrm{Li}^+$, $\\mathrm{Na}^+$ $\\mathrm{Cl}^-$. \n",
    "In addition, the solvation free energies of $\\mathrm{CO}$, $\\mathrm{H_2O}$ and $\\mathrm{Glucose}$ in different solvents will be computed. \n",
    "\n",
    "For the electrostatic contributions in equation 1, we will use the generalized Born model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 104,
   "metadata": {
    "id": "4S7N8QX_2RBM"
   },
   "outputs": [],
   "source": [
    "#Use scipy.constants for all elementary constants as in previous exercises\n",
    "import scipy.constants as sc\n",
    "\n",
    "import numpy as np \n",
    "\n",
    "#Relative perimittivities of solvents, epsilon_r: water, toluene\n",
    "epsilon_h2o = 78.3 \n",
    "epsilon_tol = 2.4\n",
    "\n",
    "R = sc.R # SI units: m^3 Pa K^-1 mol^-1 \n",
    "e = sc.e # au to Coulomb conversion factor \n",
    "epsilon0 = sc.epsilon_0 # Vacuum permitivity\n",
    "#0.5292e-10 #C from Hirshfeld to C\n",
    "\n",
    "\n",
    "#Generalized Born parameters in nanometers\n",
    "gb_param = {\n",
    "    'h': 0.850,\n",
    "    'li': 0.144,\n",
    "    'c': 0.445,\n",
    "    'o': 0.500,\n",
    "    'na': 0.186,\n",
    "    'cl': 0.181\n",
    "}\n",
    "\n",
    "\n",
    "### Free solvation energy values as reference value for checking: \n",
    "# Minessota solavtion Database (see lecture slides): https://comp.chem.umn.edu/mnsol/\n",
    "# Other source: https://link.springer.com/article/10.1007/s00214-012-1250-7 \n",
    "\n",
    "# water_in_water   = -26.4 kJ/mol\n",
    "# water_in_toluene = -7.07 kJ/mol\n",
    "# Li_in_water = -513.4 kJ/mol\n",
    "# Li_in_acetonitrile = -537.2 kJ/mol\n",
    "# glucose_in_water = -28.6kJ/mol\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "SQNkV6iF2RBN"
   },
   "source": [
    "The volume reduction for the conversion of 1 mol ideal gas to an (ideal) 1 molar solution under standard conditions (1 atm, 298 K), is considered first. Under given conditions, this value is independent of the type and form of the solute. \n",
    "An expression for this conversion can be derived using the molar Helmholtz energy $A_m$:\n",
    "\n",
    "$$\n",
    "A_m = - RT \\ln(z_i)\n",
    "$$\n",
    "where $z_i = \\gamma^{3/2}V_i$ is a partition function of the system $i$ with volume $V$ and $\\gamma$ being a constant. $R$ is the ideal gas constant and $T$ the thermodynamical temperature.\n",
    "\n",
    "\n",
    "Derive an expression for the volume work (change in Helmholtz energy) for the mentioned conversion from gas to solution and compute its value (in kJ/mol). Define all symbols used in your equations. \n",
    "Implement this standard state correction as a python function with optional arguments for the relevant quantitites."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "rvF86CUG2RBO"
   },
   "source": [
    "Next, we will use equation 2 to compute $\\delta G_{elec}$, as given in the generalized Born model:\n",
    "\n",
    "\\begin{equation}\n",
    "\\delta G_{elec} = \\frac{1}{8\\pi\\epsilon_{0}} \\left( \\frac {1-\\epsilon_{r}}{\\epsilon_{r}} \\right)\n",
    "\\sum_{A}^{N_\\text{atoms,solute}} \\sum_{B}^{N_\\text{atoms,solute}} {\\frac{q_A \\cdot q_B}{f_{GB}(r_{AB}, \\alpha_A, \\alpha_B)}}\n",
    "\\end{equation}\\tag{2}\n",
    "\n",
    "$\\epsilon_{0}$ is the dielectric constant of vacuum and $\\epsilon_{r}$ is the dielectric constant of the solvent (declared above). $q_A$ and $q_B$ are partial charges of atoms $A$ and $B$, respectively. $f_{GB}$ is the empirical screening function used in the GB model, which reads:\n",
    "\\begin{equation}\n",
    "f_{GB}(r_{AB}, \\alpha_A, \\alpha_B) = \\sqrt{ r_{AB}^{2} + \\alpha_{A} \\alpha_{B}  \\cdot exp{\\left( -\\frac {r_{AB}^{2}}{4 \\alpha_{A} \\alpha_{B}} \\right) } }\n",
    "\\end{equation}\\tag{3}\n",
    "\n",
    "$\\alpha_{A} \\cdot \\alpha_{B}$ (each in units of distance, as mentioned above) are empirical parameters. These are chosen to roughly describe the distance of the center of the atom to the solvent surface constituting the molecular cavity.\n",
    "\n",
    "Implement a function to compute the GB screening function and a function to compute $\\delta G_{elec}$. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Everything needed to compute the electrostatic contribution to the solvation free energy in the GB model is present except for atomic partial charges. In the lecture, you have learned about the Mulliken partitioning scheme to compute partial charges $q_A$ of atom $A$ defined as:\n",
    "$$\n",
    "q_A = Z_A - N_{\\mathrm{e},A} = Z_A - \\sum_{\\nu \\in A}^{N_\\mathrm{bf}} \\sum_{nu}^{N_\\mathrm{bf}} D_{\\mu\\nu} S_{\\mu\\nu}\n",
    "$$\n",
    "$Z_A$ are the nuclear charges and $N_{\\mathrm{e},A}$ is the number of electrons \"assigned\" to atom $A$, also known as Mulliken population. \n",
    "\n",
    "\n",
    "Perform Hartree-Fock calculations (STO-3G basis set) to obtain Mulliken charges for $\\mathrm{Li^+}$, $\\mathrm{CO}$ and $\\mathrm{H_2O}$ using the function `mulliken_pop()`. \n",
    "\n",
    "Compare the Mulliken partial charges to those provided in the comment line of the corresponding xyz file of the investigated system and briefly discuss similarities and differences.\n",
    "Use both partial charge schemes to compute $\\delta G_{elec}$ in kJ/mol with water as solvent. Use the provided atomic GB parameters to do so.\n",
    "\n",
    "Give two reasons why Mulliken charges may or may not be feasible for the computation of $\\delta G_{elec}$.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given below are the $G_{SASA}$ contributions for the three systems with water as solvent.\n",
    "\n",
    "Collect all results to determine $\\delta G_{solv}$ according to eq. 1 for the three investigated systems. Briefly comment on the size of each term to $\\delta G_{solv}$ and explain differences among the investigated systems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 105,
   "metadata": {},
   "outputs": [],
   "source": [
    "# All values given in Hartree\n",
    "GSASA_li_in_H2O = -0.021784473372\n",
    "GSASA_co_in_H2O = -0.003369403679\n",
    "GSASA_h2o_in_H2O = 0.000822729064"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "MkCFPBkM2RBQ",
    "outputId": "f9064c61-786c-4592-a672-1a10ca90e56a"
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Mdk4jVkBLCw3"
   },
   "source": [
    "Compute $\\delta G_{solv}$ for glucose as well using your function definitions up to this point. Here it is sufficient to only use the set of reference charges (provided below but not in the corresponding xyz file)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 106,
   "metadata": {},
   "outputs": [],
   "source": [
    "q_glucose = np.array([ 0.050598, 0.058670, 0.023631, 0.041975, -0.237740, 0.039032, 0.123486, 0.026054,-0.259155,-0.202893, 0.024048,-0.227796, 0.053967, 0.025953, 0.029978,-0.244731,-0.252901, 0.017935, 0.180045, 0.027882, 0.178165, 0.183247, 0.154304, 0.186233]\n",
    "                     )\n",
    "GSASA_glucose_in_H2O = -0.001474618146 #Hartree"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "G8FvNuIoLCw4"
   },
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "BjzWZnkeLCw4"
   },
   "source": [
    "### Task 2\n",
    "\n",
    "So far, we have calculated the free energy of solvation for some molecules in water, but in principle this can be extended to any solute and solvent we are interested in. In this task, we will look at other solutes (Toluene, THF and Acetonitrile).\n",
    "The dielectric constants for these solvents are provided below. A series of files containing partial charges from an external calculation are provided alongside the xyz files of the molecules. The file `glucose_tol.chg` contains $N_\\mathrm{atm}$ partial charges of the glucose molecule where toluene has been used to emulate the implicit solvent. The same ordering of element symbols as in the xyz file is used to write-out these charges. In the last row, the corresponding SASA contribution (again in Hartree) is given, highlighted with \"SASA:\".\n",
    "\n",
    "Write a reader function to work with the provided partial charge and SASA information.\n",
    "\n",
    "Use the given data to compute the solvation free energies of the corresponding solutes and solvents for all four systems. Briefly discuss differences for a) different partial charges (and solutes) and b) neutral vs. charged species. \n",
    "\n",
    "Compare your values against the ones generated for water as a solvent in Task 1 and explain differences."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 107,
   "metadata": {
    "id": "pUAVVqzRLCw4"
   },
   "outputs": [],
   "source": [
    "toluene_dielectric = 2.38 #Dielectric constant of Toluene\n",
    "THF_dielectric = 7.25  #Dielectric constant of THF\n",
    "CH3CN_dielectric = 37.5 #Dielectric constant of Acetonitrile"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "wYDaGhKhLCw5"
   },
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Zpa1q0dALCw6"
   },
   "source": [
    "### Task 3\n",
    "\n",
    "As a final task, calculate the free energy of solvation for $\\mathrm{Na^+}$ and $\\mathrm{Cl^-}$ with all solvents used before.\n",
    "\n",
    "The free energy of solvation for ions appears to be on another magnitude than for the non-charged molecules, most notably for polar solvents. Explain this observation based on the physical model used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 108,
   "metadata": {
    "id": "2LbumUA5LCw6"
   },
   "outputs": [],
   "source": [
    "GSASA_Na_in_acetonitrile = -0.01542349163 \n",
    "GSASA_Na_in_water = -0.012275538580 \n",
    "GSASA_Na_in_toluene = -0.006477033412 \n",
    "GSASA_Na_in_THF = -0.014041240751 \n",
    "\n",
    "GSASA_Cl_in_acetonitrile = -0.004539782458 \n",
    "GSASA_Cl_in_water = -0.000221933034 \n",
    "GSASA_Cl_in_toluene = -0.002560078983 \n",
    "GSASA_Cl_in_THF = -0.005751831357 "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "myvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
