{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# COS 3a Exercise 3\n",
    "---\n",
    "Submission by 29/10/2024 12:00 p.m\n",
    "\n",
    "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>\n",
    "\n",
    "## Tutorial\n",
    "\n",
    "### Computation of the Hessian Matrix\n",
    "\n",
    "The Hessian matrix $\\mathbf{H}$ contains the second derivatives of the electronic energy $E$ with respect to the cartesian coordinates of the respective atoms $i$ and $j$.\n",
    "\n",
    "$$\n",
    "\\mathbf{H}_{ij} = \\frac{\\partial^2 E}{\\partial \\alpha_i \\partial \\alpha_j}, \\alpha \\in \\lbrace x,y, z\\rbrace\n",
    "$$\n",
    "\n",
    "For this exercise, we will denote 'the' Hessian matrix simply as Hessian, which commonly refers to the energy derivative with respect to cartesian coordinates. There are other 'Hessian matrices' (second derivatives of the energy w.r.t to other quantities) that we will discover in later exercises.\n",
    "\n",
    "From the above definition, we can represent the Hessian as a $3N \\times 3N$ matrix with $N$ being the number of atoms of our system:\n",
    "$$\n",
    "\\begin{pmatrix}\n",
    "\\frac{\\partial^2 E}{\\partial x_1 \\partial x_1} & \\frac{\\partial^2 E}{\\partial x_1 \\partial y_1} & \\ldots & \\frac{\\partial^2 E}{\\partial x_1 \\partial z_N} \\\\\n",
    "\\frac{\\partial^2 E}{\\partial y_1 \\partial x_1} & \\frac{\\partial^2 E}{\\partial y_1 \\partial y_1} & \\ldots & \\frac{\\partial^2 E}{\\partial y_1 \\partial z_N} \\\\\n",
    "\\vdots & \\vdots & \\ddots & \\vdots \\\\\n",
    "\\frac{\\partial^2 E}{\\partial z_N \\partial x_1} & \\frac{\\partial^2 E}{\\partial z_N \\partial y_1} & \\ldots & \\frac{\\partial^2 E}{\\partial z_N \\partial z_N} \\\\\n",
    "\\end{pmatrix}\n",
    "$$\n",
    "In the following, the code to compute the Hessian matrix of water in `pyscf` is provided. Take a look at the syntax and how a molecule is constructed. Check if you can recognise the definition of the hessian in the reshaped variable `h`, e.g. by printing out (pieces of) the matrix and checking its dimensions. What matrix properties do you expect for the Hessian?  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import pyscf modules required to compute Hessian \n",
    "from pyscf import gto, scf, hessian"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of a water molecule \n",
    "# Atom positions are declared in Angstrom\n",
    "# Basis set for Hartree-Fock calculation is the 6-31g Pople basis set\n",
    "\n",
    "mol = gto.M(\n",
    "    atom = [\n",
    "        ['O' , 0. , 0.     , 0],\n",
    "        ['H' , 0. , -0.757 , 0.587],\n",
    "        ['H' , 0. ,  0.757 , 0.587]],\n",
    "    basis = '631g')\n",
    "\n",
    "# Energy calculation with PySCF\n",
    "rhf = scf.RHF(mol)\n",
    "rhf.kernel() \n",
    "\n",
    "# Computation of Hessian, matrix reshaped to 3N * 3N and saved as h\n",
    "hess = hessian.RHF(rhf)\n",
    "h = hess.kernel()\n",
    "h = h.transpose(0,2,1,3).reshape(3*mol.natm, 3*mol.natm)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1\n",
    "\n",
    "Compute the atomic masses from the elements of the `mol` object and mass-weight (MW) the Hessian accordingly:\n",
    "$$\n",
    "H_{ij}^\\mathrm{MW} = \\frac{H_{ij}}{\\sqrt{m_i m_j}}\n",
    "$$\n",
    "You can use `mol.atom` to access atomic information declared in `mol`. \n",
    "\n",
    "<u> Optional:</u> Think about how this routine can be performed using matrix operations. What would be a suitable array (vector or matrix) to represent the masses for this operation? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2\n",
    "\n",
    "We can determine the harmonic frequencies of a molecule from the MW Hessian, since these are proportional to the eigenvalues $\\lambda$. In order to compute the eigenvalues, the Hessian needs to be diagonalized first. We will use [`np.linalg.eigh`](https://numpy.org/doc/2.0/reference/generated/numpy.linalg.eigh.html) for this step.\n",
    "\n",
    "From the lecture you know that a diagonalization corresponds to the transformation of a matrix that yields\n",
    "$$\n",
    "\\mathbf{H} = \\mathbf{Q} \\boldsymbol{\\lambda} \\mathbf{Q}^T\n",
    "$$\n",
    "with the eigenvectors stored in the matrix $\\mathbf{Q}$ and the diagonal matrix $\\boldsymbol{\\lambda}$ that contains the eigenvalues $\\lambda_i$.\n",
    "\n",
    "Compute the eigenvalues of the MW Hessian. \n",
    "\n",
    "Transform $\\mathbf{Q}$ such that the eigenvectors correspond to the columns of the matrix and show that the eigenvectors are normalized.\n",
    "\n",
    "Compute the vibrational frequencies $\\nu_i$ in units of $\\mathrm{cm}^{-1}$ and print them to the terminal with a reasonable number of significant digits. Use that:\n",
    "$$\n",
    "\\nu_i = \\frac{\\sqrt{|\\lambda_i^\\prime|}}{2 \\pi c} \\,.\n",
    "$$ \n",
    "Note that the MW Hessian is given in atomic units (au), that is $\\mathrm{\\frac{\\mathrm{E_h}}{a_0^2 u}}$ where $\\mathrm{E_h}$ are Hartree, $u$ the atomic mass unit and $a_0$ the Bohr radius. Write a function that performs the correct unit conversion from atomic units (au) $\\lambda_i$ to the corresponding SI unit $\\lambda_i^\\prime$. Take all the required elementary constants for this task from [`scipy.constants`](https://docs.scipy.org/doc/scipy/reference/constants.html) (see below). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import scipy constants\n",
    "import scipy.constants as sc\n",
    "\n",
    "# Take Planck constant directly\n",
    "print(sc.h)\n",
    "\n",
    "# Look up 'less common' constants \n",
    "print(sc.find(\"bohr\"))\n",
    "# Get scipy dictionary entry for this constant\n",
    "print(sc.physical_constants['Bohr radius'])\n",
    "a0 = sc.physical_constants['Bohr radius'][0] # Bohr radius in meter\n",
    "print(a0)\n",
    "\n",
    "# ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 3 \n",
    "\n",
    "Taking the frequencies from Task 2, we now want to compute the free energy of vibration $G_\\mathrm{vib}$ for our molecule. From statistical thermodynamics, one can derive an expression for the enthalpy and entropy of vibration from the partition function $z_\\mathrm{vib}$. One arrives at:\n",
    "$$\n",
    "S_\\mathrm{vib} = -R \\sum_i \\left( 1 - \\exp(-\\epsilon_i / k_\\mathrm{B} T ) \\right) + R \\sum_i \\frac{\\epsilon_i}{k_\\mathrm{B} T} \\cdot \\frac{\\exp(-\\epsilon_i / k_\\mathrm{B} T)}{ 1 - \\exp(-\\epsilon_i / k_\\mathrm{B} T)}\n",
    "$$\n",
    "Here we make use of the information provided on this [website](https://cccbdb.nist.gov/thermox.asp) - a convenient reference for reading about the computation of nuclear contributions to thermodynamic properties. The enthalpy reads:\n",
    "$$\n",
    "H_\\mathrm{vib} = RT \\sum_i \\frac{\\epsilon_i}{k_\\mathrm{B} T} \\cdot \\frac{\\exp(-\\epsilon_i / k_\\mathrm{B} T)}{ 1 - \\exp(-\\epsilon_i / k_\\mathrm{B} T)}\n",
    "$$\n",
    "\n",
    "Take the above expression and create functions to compute the respective contributions to the free energy of vibration $G_\\mathrm{vib}$.\n",
    "\n",
    "Briefly explain how the above expressions for $S_\\mathrm{vib}$ and $H_\\mathrm{vib}$ are obtained from the partition function, e.g. in a few sentences or using equations. \n",
    "\n",
    "Take the above water molecule and compute $H, S, G$ using only frequencies $>200~\\mathrm{cm^{-1}}$ for a temperature range between 50 and 200 °C and plot the data. Make sure to format the plot and label the data.    \n",
    "\n",
    "Briefly explain why it is necessary to exclude low frequencies from the computation. What do these correspond to in a physical sense?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "myvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
