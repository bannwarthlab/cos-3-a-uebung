{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 9\n",
    "\n",
    "---\n",
    "Submission until 17/12/2024 12:00 p.m.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "2qhCnjYR2RBK"
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Molecular Dynamics \n",
    "\n",
    "In the lecture, some integrators to solve the ordinary differential equation (ODE) associated with the equation of motion (EOM) were presented. In the context of molecular dynamics (MD) simulations, we want to describe this motion of a (classical, molecular) system over time. \n",
    "One of the integrators we will concentrate on in this exercise is the so-called [Leapfrog](https://en.wikipedia.org/wiki/Leapfrog_integration) algorithm. In general, the EOM is an ODE that can be written as: \n",
    "$$\n",
    "\\dot{v} = \\frac{d v(x)}{dt} = A(x)\n",
    "$$\n",
    "The Leapfrog algorithm now proposes a numerical solution to this ODE by updating velocities at half time steps and positions at full time steps, namely:\n",
    "\\begin{align}\n",
    "a_i = A(x_i) \\\\\n",
    "v_{i+1/2} = v_{i-1/2} + a_i \\Delta t \\\\\n",
    "x_{i+1} = x_i + v_{i+1/2} \\Delta t\n",
    "\\end{align}\n",
    "where $a_i$ is the acceleration, $x_i$ the position and $v_i$ the velocity at time step $i$. \n",
    "Alternative variants for integrators are the (Velocity) Verlet algorithm, that have been introduced briefly in the lecture as well.\n",
    "\n",
    "### Interatomic Potentials\n",
    "\n",
    "Since we need to model the potential energy surface (PES) of our system, we need to express the energy $E$ as a function of atom positions in some way. For the purpose of this exercise, we will use a trivial harmonic model potential - the harmonic oscillator potential - and provide a more involved mixed potential comprising intramolecular bond and angle terms ([force-field](https://en.wikipedia.org/wiki/Force_field_(chemistry)) like potential) and an intermolecular [Lennard-Jones](https://en.wikipedia.org/wiki/Lennard-Jones_potential) potential, which you can optionally expand to also involve Coulomb interactions. \n",
    "In principle you can carry out MD simulations with arbitrarily complex molecular systems given that a description of the PESs can be accomplished. This can range, for example, from force-field MDs of large biomolecules to $ab~initio$ MDs of small(er) molecules or condensed systems. The essential quantitites you need to set up an MD simulation are the nuclear forces (acceleration) and an integrator letting you propagate your system in time. The atom positions and velocities of the particles need to be updated.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1\n",
    "\n",
    "In this exercise, we want to implement the Leapfrog algorithm for a simple 1D harmonic oscillator (HO) potential given as:\n",
    "$$\n",
    "U(x) = -\\frac{1}{2} k x^2\n",
    "$$\n",
    "where $U$ is the potential energy and $k$ is the \"spring\" or harmonic constant and $x$ is the (displacement from the equilibrium) position. \n",
    "\n",
    "Derive an expression to compute the force (see also lecture) of the harmonic potential. \n",
    "\n",
    "Implement a function to compute the harmonic potential and the nuclear force. Use the standard input parameters given below as **optional** arguments for your function. \n",
    "\n",
    "Based on this model system, implement the Leapfrog algorithm to propagate this 1D particle in time. Think of reasonable starting conditions for your model systems.\n",
    "\n",
    "Visualize the particle position and velocity in a plot and briefly describe what you see (max 2 sentences)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dt = 0.01  # Time step (a.u.)\n",
    "k = 8.0 # Harmonic constant\n",
    "N = 1000 # Number of steps to propagate the system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2\n",
    "\n",
    "### The Model System\n",
    "\n",
    "For this task, we try to adapt the model presented in [this article](https://www.sciencedirect.com/science/article/pii/S0378381207005912). We will simulate water molecules in a box with periodic boundary conditions (PBC) enabled. The PES will be described using intra- and intermolecular interactions. The code needed to work on this exercise is provided in `ljpot.py` and the general functionality is presented below.\n",
    "\n",
    "The expression for the intramolecular potential reads:\n",
    "\\begin{align}\n",
    "U_\\mathrm{intra} = \\frac{1}{2} K_r (r-r_0)^2 + \\frac{1}{2} K_\\theta (\\theta - \\theta_0)^2\n",
    "\\end{align}\\tag{6}\n",
    "whereas the interatomic potential reads:\n",
    "\\begin{align}\n",
    "U_\\mathrm{inter}(r_{ij}) = 4\\epsilon_{ij} \\left[ \\left(\\frac{\\sigma_{ij}}{r_{ij}}\\right)^{12} - \\left(\\frac{\\sigma_{ij}}{r_{ij}}\\right)^{6} \\right] + \\frac{q_i q_j}{4 \\pi \\epsilon_0 r_{ij}}\n",
    "\\end{align}\\tag{7}\n",
    "As can be seen, we have expressions in the intramolecular potential that resemble a molecular force-field, where a bond distance and angle term are contained. \n",
    "The 6-12-LJ intermolecular potential contains an additional Coulomb term with fixed atomic partial charges. \n",
    "\n",
    "In order to run MD simulations for a system whose potential energy can be described by the sum of these two contributions, we need to compute the nuclear forces acting on each particle. The code for these is provided and the working equations can, in principle, be obtained via partial derivation of the potential terms w.r.t. the cartesian coordinates of the respective particle.\n",
    "\n",
    "To begin with, we define all constants in the following and define our system of water molecules. Table 1 of the mentioned article reads:\n",
    "|    Parameter   | Value          |   \n",
    "|----------------|--------------\n",
    "| $R_\\mathrm{OH}~(\\mathrm{\\AA})$       | 0.9611        \n",
    " | $K_r~(\\mathrm{kJ\\cdot mol^{-1} \\cdot\\AA^{-2}})$ | 1480              |\n",
    "| $\\theta_\\mathrm{HOH}~(°)$       | 109.47         \n",
    "| $K_\\theta~(\\mathrm{kJ\\cdot mol^{-1} \\cdot\\AA^{-2}})$  | 353               |\n",
    "| $q_\\mathrm{H}~(\\mathrm{e})$         | 0.4238        \n",
    "| $q_\\mathrm{O}~(\\mathrm{e})$         | −0.8476        \n",
    "| $\\epsilon_\\mathrm{H}~(\\mathrm{kJ/mol})$     | 0.0324             |\n",
    "| $\\epsilon_\\mathrm{O}~(\\mathrm{kJ/mol})$     | 0.6287             |\n",
    "| $\\sigma_\\mathrm{H}~(\\mathrm{\\AA})$        | 0.98               |\n",
    "| $\\sigma_\\mathrm{O}~(\\mathrm{\\AA})$       | 3.1169       \n",
    "\n",
    "These parameters are defined (in different units) below to be used for the simulations.\n",
    "\n",
    "### NVT Ensemble and Velocity Rescaling \n",
    "\n",
    "In our MD simulation, we want to describe a NVT ensemble, that is, an ensemble where the particle number $N$, the volume $V$ and the thermodynamic temperature $T$ at each step is preserved. The former are satisfied based on the model we choose. To preserve the thermodynamic temperature, we re-adjust $T$ at each time step in the MD simulation by rescaling the particle velocities $v_i$. \n",
    "The (time-)averaged kinetic energy $\\braket{K}$ of the $N$ particle ensemble reads:\n",
    "$$\n",
    "\\braket{K} = \\frac{3}{2}N k_\\mathrm{B}T\n",
    "$$\n",
    "The temperature of our system can now be conserved by constraining the averaged kinetic energy per particle, but allowing the overall kinetic energy $K = \\frac{1}{2}\\sum_i^N m_iv_i^2$ to vary. The idea of velocity rescaling and thermostats for MD simulations is also outlined in more detail [here](https://www.compchems.com/thermostats-in-molecular-dynamics/#velocity-rescaling).\n",
    "\n",
    "\n",
    "### Exercises \n",
    "\n",
    "Familiarize yourself with the example usage of the model and code to run the MD simulations and add comments to the example below.  \n",
    "\n",
    "Write brief explanations about the functions provided in `ljpot` (max 3 sentences each). <span style=\"color:red\"> Do not just copy & paste the function description! </span>\n",
    "\n",
    "Analyze the influence of 3 different simulation parameters on the MD simulation and describe your findings with bullet points in markdown. Support your analysis with useful plots. Here is a list of options to choose from, but own ideas are much appreciated:\n",
    "\n",
    "- Simulation Temperature $T$, Box volume $V$, Particle Number $N$\n",
    "- Usage of Periodic Boundary Conditions\n",
    "- Temperature scaling function\n",
    "- Neglecting different terms to model the potential energy (e.g. omitting Coulomb potential in $U_\\mathrm{inter}$)\n",
    "- Varying model parameters that affect the energy, e.g. $q_i$, $r_\\mathrm{OH}$\n",
    "\n",
    "Give three suggestions on how the MD simulation can be improved in regard to, e.g. the (model) accuracy, performance, or other aspects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import modules for this exercise\n",
    "import ljpot as ljp\n",
    "\n",
    "# Model constants\n",
    "r_oh = 0.9611 # Angstrom\n",
    "k_r = 1480.0 / 2 # Involves factor of 1/2 from potential expression and unit rescaling \n",
    "theta_hoh = np.radians(109.47) # Angle in radians\n",
    "k_theta = 353.0 / 2 \n",
    "\n",
    "# Epsilon and sigma matrices containg atom and pair parameters according to eq. 3 and 4, readjusted\n",
    "eps_h = 0.0324 \n",
    "eps_o = 0.6287\n",
    "sig_h = 0.98\n",
    "sig_o = 3.1169\n",
    "\n",
    "eps = np.array([\n",
    "    [eps_h + eps_h, eps_h + eps_o], \n",
    "              [eps_h + eps_h, eps_o + eps_o]\n",
    "              ])\n",
    "eps *= 0.5 # Eq. 3 in article\n",
    "\n",
    "\n",
    "sig = np.array([\n",
    "    [sig_h * sig_h, sig_h * sig_o], \n",
    "              [sig_h * sig_o, sig_o * sig_o]\n",
    "              ])\n",
    "sig = np.sqrt(sig) # Eq. 4 in article\n",
    "\n",
    "# Masses and charges of atom types\n",
    "mass_ = np.array([1.0, 16.0])\n",
    "qs_ = np.array([0.4238, -0.8476])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define physical system to perform MD simulation with\n",
    "\n",
    "n = 21 # Number of particles\n",
    "D = 3 # Dimensionality of the system \n",
    "dt = 0.0001 # Time step (in ps)\n",
    "blength = 20.0 # Molecular box volume (in Angstrom)\n",
    "\n",
    "#Initialize random positions and velocities of particles\n",
    "np.random.seed(1000)\n",
    "x_init = np.random.rand(n, D) * blength \n",
    "v_init = np.random.rand(n, D) - 0.5 \n",
    "\n",
    "T = 300 # Simulation temperature (in K) \n",
    "\n",
    "#set dimensions of box where MD is run in\n",
    "L = np.zeros([D]) + blength"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Helper function to construct the water model system based on initial paramters  \n",
    "model = ljp.build_model(nmol = n,\n",
    "                bdist= r_oh,\n",
    "                kbond= k_r,\n",
    "                theta= theta_hoh, \n",
    "                ktheta = k_theta,\n",
    "                ms = mass_,\n",
    "                qs = qs_)\n",
    "\n",
    "# Extract quantities for simulation from model dictionary\n",
    "#bonds = model['bonds']\n",
    "#angles = model['angles']\n",
    "#atomtypes = model['attypes']\n",
    "#molecules = model['mols']\n",
    "#masses = model['masses']\n",
    "#charges = model['charges']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ljp.run_MD(model = model,\n",
    "            xin = x_init,\n",
    "            vin= v_init,\n",
    "            dt = dt,\n",
    "            L = L,\n",
    "            sigma= sig,\n",
    "            epsilon= eps,\n",
    "            simT= T,\n",
    "            nstep=200,\n",
    "            nskip=20,\n",
    "            dumpdir=\"_dumps\",\n",
    "            doPBC=False\n",
    "            )"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "exvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
