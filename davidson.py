### Contains functions to use in exercise 12 ### 
# Ref: https://joshuagoings.com/2013/08/23/davidsons-method/

import numpy as np
from typing import Union
import time

def Davidson(A : np.array, 
             neval : int, 
             convtol : float = 1e-5, 
             maxiter : int = 1000, 
             upper : bool = False, 
             printinfo : bool = False, 
             custT : Union[np.array,None] = None, 
             guessmult : int = 3) -> tuple[np.array, np.array, float]:
    """ Implementation of a Davidson eigensolver to find some eigenvalues and eigenvectors of a symmetric matrix A.
    
    
        Args:
            A (np.array): The (large) symmetric matrix to determine the eigenvalues and -vectors for. Dim: N, N.
            neval (int): Number of targeted eigenvalues to determine iteratively.
            convtol (float): Convergence tolerance for the algorithm. Def.: 1e-5.
            maxiter (int): Maximum number of iterations for the solver. Def.: 1000.
            upper (bool): Whether to determine fewest neval lower or upper eigenvalues and -vectors. Defaults to False (=> Lowest).
            printinfo (bool): Print additional info of run.
            custT (np.array | None): If not None, a set of custom trial vectors can be passed here. Def.: None (=> Use built-in construction of set of trial vectors)
            guessmult (int): Integer that defines multiples of neval to be chosen to construct the inital guess space. Def.: 3. 

        Returns:    
            omega (np.array): Approx. neval eigenvalues of A. Dim: neval.
            x_ (np.array): Approx. eigenvectors of A. Dim: (neval, N)
            Time spend for execution (float). In seconds.
  
    """

    N = A.shape[0]

    mmax = min(N//2, maxiter)
    start = time.time()

    if upper: # Handle selection of eval spec 
        inv = -1.0
    else:
        inv = 1.0

    eig = min(neval,N) # number of eignvalues to solve  
    
    k = min(guessmult * neval,N) # number of guess vectors
    if printinfo:
        print(f"Using {k} initial guess vectors.")
    # set of k unit vectors as guess
    t = np.zeros((N,k))
    
    idx = np.diag(inv*A).argsort()

    if custT is None:
        for i in range(0,k): # Construct set of orthonormal guess vectors
            t[idx[i],i] = 1.0
    else:
        print("Using custom trial vectors.")
        t = custT.copy()
    assert t.shape[0] == N and t.shape[1] >= neval, "Shape of trial vectors wrong. Should be dimension of A matrix x number of request eigenvalues or larger."

    V = np.zeros((N,N)) # array to hold trial vector space  
    I = np.eye(N) # identity matrix with dim(A)

    theta = np.zeros(k)
    norm=0.0

    # go through iterations in steps of k (desired eigenpairs + additional guesses)
    for m in range(k,mmax,k): 
        if printinfo:
            print(f"Iteration: {m}, Norm: {norm}")
        if m <= k:
            # first iteration: initialize the trial vectors
            for j in range(0,k):
                V[:,j] = t[:,j]/np.linalg.norm(t[:,j]) # Ensure normalized t
            theta_old = 1 
        elif m > k:
            # update eigenvalues
            theta_old = theta[:eig]
        
        # orthogonalize guess vectors with QR decomposition
        V[:,:m],_ = np.linalg.qr(V[:,:m])
        # tranform matrix to basis of guess vectors
        T = np.dot(V[:,:m].T,np.dot(A,V[:,:m]))
        if printinfo:
            print(f"Subspace A (dim: {T.shape}): {T}")
        # diagonalize 
        THETA,S = np.linalg.eig(inv*T)

        # sort eigenvalues and vectors
        idx = THETA.argsort()
        theta = inv*THETA[idx]
        s = S[:,idx]

        if(m+k<=N):
           # get residual array and append to the guess matrix
           for j in range(0,k):
               # guess vector for large matrix
               w = np.dot((A - theta[j]*I),np.dot(V[:,:m],s[:,j])) 
               q = w/(theta[j]-A[j,j])
               # print(j,m)
               V[:,(m+j)] = q
        norm = np.linalg.norm(theta[:eig] - theta_old)
        if norm < convtol:
            break
    
    end = time.time()
    if printinfo:
        print(f"Time Davidson solver for {neval} roots: {end - start:.4f} s")
        print(f"Used {m} iterations.")

    omega = theta[:eig]
    x_ = V[:,:eig]

    return omega, x_, end - start





