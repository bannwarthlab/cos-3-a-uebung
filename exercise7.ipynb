{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 7\n",
    "\n",
    "---\n",
    "Submission until 03/12/2024 12:00 p.m.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "2qhCnjYR2RBK"
   },
   "outputs": [],
   "source": [
    "import random\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Introduction to Monte Carlo\n",
    "\n",
    "In general, we can define a Monte Carlo (MC) method as a numerical simulation technique that relies on the generation of random numbers to estimate integration values and probabilistic quantities. This family of methods finds application in solving problems that cannot be solved analytically or with deterministic methods.\n",
    "\n",
    "Regardless of the MC method considered, we can describe three basic steps:\n",
    "- Set up a predictive model\n",
    "\n",
    "- Define the probability distributions of the independent variables\n",
    "    \n",
    "- Run repeated simulations with the random values for the independent variables\n",
    "\n",
    "### Random walk\n",
    "\n",
    "A basic example for MC simulations is the one-dimensional random walk. This is a stochastic process consisting of a sequence of steps in a one-dimensional space. Each step is randomly generated from the previous point, but is completely independent of the history of the walk.\n",
    "\n",
    "This method can be applied to a wide range of situations with more complete models, such as the movement of particles in a fluid, stock prices in the market, and gambling behavior.\n",
    "\n",
    "### Metropolis Monte Carlo Algorithm\n",
    "\n",
    "The Metropolis Monte Carlo (MMC) method is a specific random walker algorithm, which proposes random steps and accepts or rejects them according to a selection criterion that guarantees convergence to the desired probability distribution.\n",
    "\n",
    "The Metropolis-Hastings (MH) algorithm allows us to sample an *a priori* distribution $p(x)$, if we know at least the function that generates that distribution. The MH algorithm is based on the normal MMC algorithm, but it changes the way in which the relationship with the new value is generated, because the Hastings version does not require the distribution to be symmetric.\n",
    "\n",
    "The algorithm can be expressed in following steps:\n",
    "\n",
    "1. From the proposal distribution $D(x^{0}|x_{i})$, extract one value $x^{0}$\n",
    "\n",
    "2. Compute the ratio between $x^{0}$ and the current step $x^{*} $ as $\\omega = \\frac {D(x_{i}|x^{*})f(x^{*})} {D(x^{*}|x_{i})f(x_{i})}$\n",
    "\n",
    "3. If $\\omega ≥ 1$, then $x^{*} = x_{i+1}$\n",
    "\n",
    "4. If $ \\omega < 1 $, admit the possibility that $x^{*} = x_{i+1}$ generating a random factor $r \\in [0,1]$:\n",
    "    - if $ r < \\omega$, accept new value and set $x^{*} = x_{i+1}$\n",
    "    - if $ r \\geq \\omega$ then $x_{i+1} = x_{i}$  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "xzfs29og2RBQ"
   },
   "source": [
    "### Task 1\n",
    "\n",
    "The goal of the first task is to use the MMC algorithm to sample a function $f(x)$ using different numbers of sample points $n$. We will create a histogram to visualize the function produced by the given MMC simulation.\n",
    "\n",
    "The MMC algorithm will generate random values but accept them with higher probability since the region of the distribution has a high probability.\n",
    "\n",
    "The function whose area we want to replicate has the following form:\n",
    "\n",
    "\\begin{equation}\n",
    "f(x) = 10e^{-4(x+4)^2} + 3e^{-0.2(x+1)^2} + e^{-2(x-5)^2}\n",
    "\\end{equation}\n",
    "\n",
    "https://www.algorithm-archive.org/contents/metropolis/metropolis.html\n",
    "\n",
    "Implement a python function to compute eq. 1.\n",
    "\n",
    "Write a routine that allows to compute the integral of the function for $x\\in [-10,10]$ following these steps:\n",
    "\n",
    "0. Generate starting point $f(x_0)$ using a uniform distribution in the interval to be considered\n",
    "\n",
    "1. Generate new point $x_{i+1} = x_i + \\text{random([0,1])}$ with 1D random walker, drawing randomly from a uniform distribution in the interval $[0,1]$\n",
    "\n",
    "2. Calculate acceptance probability $\\omega = \\min\\left( 1, \\frac {f(x_{i+1})}{f(x_{i})} \\right)$\n",
    "\n",
    "3. Generate random number $r \\in [0,1]$.\n",
    "\n",
    "4. If $\\omega > r$ ,keep new position and set $f(x_i) = f(x_{i+1})$, otherwise set $f(x_i) = f(x_i)$\n",
    "\n",
    "5. Repeat steps 1 to 4 $n$ times.\n",
    "\n",
    "Use your MMC algorithm to sample the distribution with n = 50, 100, 500 and visualize the computed area using histograms (`plt.bar`). \n",
    "\n",
    "Briefly describe your results for different numbers of sampling points. Comment on the effect of $x_0$ and $n$ on the outcome of the simulation. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "UYUQutUM2RBR",
    "outputId": "ed71163e-dc07-4b1b-81e7-d036d537504e"
   },
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "bmnQbHvvnBQZ"
   },
   "source": [
    "### Task 2 \n",
    "<u> Note:</u> It is sensible to read the entire exercise once before you start coding. \n",
    " \n",
    "We want to find the equilibrium energy of particles on a  1D lattice using again a single random walker. For convenience, we assume a 1D chain of particles $A$ and $B$ where each position is occupied by either **A** or **B**. Furthermore, only direct neighbour interactions, are considered. The last thing we want to account for are periodic boundary conditions. This means that the particle on the far right interacts with the particle on the far left.\n",
    "\n",
    "Some starting variables for this system (temperature, lattice size, interaction energies) for particle pairs of $A$ and $B$ are provided below. These assumptions allow to define the overall lattice energy $E_\\mathrm{latt}$ as:\n",
    "\\begin{equation}\n",
    "E_{latt} = \\sum_{i = 1}^{N} \\frac{\\epsilon_{i-1,i} +\\epsilon_{i,i-1}}{2}\n",
    "\\end{equation}\\tag{2}\n",
    "since the energy of all $N$ particles $i$ is just the average of the direct neighbour pair interaction $\\epsilon_{ij}$, where $j = i-1, i+1$. \n",
    "\n",
    "\n",
    "Write a function that computes the lattice energy from a vector input that encodes the 1D lattice of (randomly) distributed particles $A$ and $B$. \n",
    "\n",
    "Use the definition of the lattice energy to realize an implementation of an MMC algorithm to find a solution to the equlibrium energy. To do so, implement the following necessary steps:\n",
    "\n",
    "1. Define the (random) 1D lattice.\n",
    "\n",
    "2. Calculate the initial lattice energy of the system $E_0$ using your previously defined function.\n",
    "\n",
    "3. Randomly select two positions on the lattice and propose an exchange of particle positions on the lattice. \n",
    "\n",
    "4. Calculate the new energy of the system $E_1$.\n",
    "\n",
    "5. Use the MMC selection criterion: $\\exp\\left(-\\frac{\\Delta E}{k_\\mathrm{B}\\cdot T}\\right) > Z$ \n",
    "to check, if the particle swap is to be accepted or not. $Z\\in [0,1]$ is a random number.\n",
    "    * If the jump is accepted, update the lattice energy and store the information in `MCS` and `ehist` (see below)\n",
    "    * If the jump is rejected, maintain the original lattice\n",
    "\n",
    "6. Repeat this process until convergence or the maximum number of steps `nmax` for your simulation is reached.  \n",
    "\n",
    "In addition, please include these \"optional\" steps to analyze the results of your MMC simulation (optional here means that these are in principle not necessary to realize the plain MMC algorithm, but you should realize it in your code!):\n",
    "\n",
    "7. Define variables `MCS` and `ehist` that record how many 'particle swaps' were accepted and track the lattice energy progression in the MMC simulation respectively.\n",
    "\n",
    "8. Add an additional threshold of MCS > 5e4 as termination criterion.\n",
    "\n",
    "9. Summarize and prinout the results of your MMC routine. Provide the initial and equilibrium lattice energy (if the latter is obtained), or write-out that the simulation did not converge. \n",
    "\n",
    "Perform a set of simulations for the lattice: \n",
    "\n",
    "`exercise_lattice = np.random.choice([0, 1], size=1000)`\n",
    "using the random seed \"2000\" (`np.random.seed(2000)`)\n",
    "\n",
    "Determine reasonable settings to reliably converge towards the equilibrium energy of the system and briefly describe how you determined those.\n",
    "\n",
    "Modify `e_aa = 0.20` and repeat your simulations. Describe the effect this modification has on your MMC simulations.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define direct neighbour interaction energies \n",
    "e_aa = 0.42 # A-A interaction in eV\n",
    "e_bb = 0.42 # B-B interaction in eV\n",
    "e_ab = 0.56 #i A-B interaction in eV\n",
    "energy_table = np.array([e_aa, e_ab, e_bb])\n",
    "\n",
    "# Simulation conditions\n",
    "import scipy.constants as sc\n",
    "Tsim = 800\n",
    "kb = sc.k / sc.electron_volt # in eV/ K "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "example_lattice = np.array([ 1, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "d-_-a_NOnBQa",
    "outputId": "0a50fa6e-0047-4bde-fb20-4cd374477713"
   },
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "lngzoUiMnBQb"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Heat Maps\n",
    "\n",
    "For the following task, we wish to visualize a 2D lattice. To do so, we can use *heatmaps* with the [`plt.imshow`](https://matplotlib.org/api/pyplot_api.html#matplotlib.pyplot.imshow) function. Below you can find a small example to make you familiar on how to use heatmaps for the upcoming MC simulation. Behind the scenes, there is some interpolation happening for color coding of the matrix elements, but for the purpose of this exercise, you need not care about that. If you are interested in that, you can follow the link for more information. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "id": "RsJ5M0R9nBQb",
    "outputId": "1cc8e9bc-d0bb-40fd-a2bd-06287495623c"
   },
   "outputs": [],
   "source": [
    "# Example matrix to visualize 4x4 matrix\n",
    "md_list = np.array([\n",
    "    [0,1,2,1], \n",
    "    [1,0,1,2], \n",
    "    [0,1,0,1],\n",
    "    [2,0,0,0]]\n",
    ")\n",
    "plt.imshow(md_list)\n",
    "plt.axis(\"off\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Fa9NvRY-nBQb"
   },
   "source": [
    "### Task 3\n",
    "\n",
    "The last task will essentially expand the previous example of a 1D lattice to two dimensions. In this system, we have to consider four direct neighbour interactions per particle. Analogous to task 2, the lattice energy now takes the form (eq. 3):\n",
    "\n",
    "\\begin{equation}\n",
    "\\varepsilon_{lattice} = \\sum_{i = 1}^{n},\\sum_{j = 1}^{m} \\frac{ \\epsilon_{(i-1)j,ij} + \\epsilon_{(i+1)j,ij} + \\epsilon_{i(j-1),ij} + \\epsilon_{i(j+1),ij} }{2}\n",
    "\\end{equation}\\tag{3}\n",
    "\n",
    "where n and m are the dimensions of the 2D lattice, which is a square matrix in our case. $\\epsilon_{(i-1)j,ij}, ...$ represent the energies associated with the direct neighbour particles on the left, right, top and bottom of the considered particle located at position $(i,j)$.\n",
    "\n",
    "In this task we will encode a two-step random walker, which means that for each step we will exchange two random particles at the same time.\n",
    "\n",
    "Implement a new function to calculate the 2D lattice energy taking a matrix encoding the fully occupied lattice, again only containing A,B particles, as input. \n",
    "\n",
    "Implement the code necessary to do a MMC simulation of the 2D lattice and adapt the output such that the lattice position is given (per MMC step or just initial and final configuration). Visualize the intial and final configuration of the 2D lattice as heatmaps. Use appropriate settings to minimize (or equilibrate) the lattice energy.\n",
    "\n",
    "Briefly state how the final configuration of the 2D lattice is affected, if the values for the pair interactions ($E_{AA}, E_{BB} \\leftrightarrow E_{AB}$) are changed. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write Your solution here\n"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "3.8",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
