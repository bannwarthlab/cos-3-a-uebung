{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "EXx-DTzxks-e"
      },
      "source": [
        "# COS 3a Exercise 4\n",
        "---\n",
        "Submission by 05/11/2024 12:00 p.m\n",
        "\n",
        "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>\n",
        "\n",
        "## Tutorial\n",
        "\n",
        "### Projection of Translational and Rotational Degrees of Freedom\n",
        "\n",
        "In exercise 3, we learned about how the harmonic frequencies of a molecule can be computed using the Hessian. Diagonalizing the Hessian yielded the eigenvalues $\\lambda_i$:\n",
        "$$\n",
        "\\mathbf{H} = \\mathbf{W} \\boldsymbol{\\lambda} \\mathbf{W}^T\n",
        "$$\n",
        "Here we use $\\mathbf{W}$ (not $\\mathbf{Q}$) to denote the eigenvectors for reasons that will become clear below. However, in mass-weighted (MW) coordinates, one generally finds that the $3 N_\\mathrm{nat}$ degrees of freedom mix. Hence, the $3 N_\\mathrm{nat}$ eigenvalues obtained from diagonalizing the Hessian include translational (T) and rotational (R) degrees of freedoms (DOF), whereas we are usually concerned with the $3 N_\\mathrm{nat} -6 (5)$ vibrational (V) degrees of freedom.\n",
        "\n",
        "In the lecture you have learned about one way to remove the TR DOF via projection onto a set of (internal) coordinates where the TR DOF are separable from the V DOF. There are several steps involved in this procedure and in this exercise, you will come up with a complete projection routine that allows you to do a proper harmonic frequency analysis from only the V DOF - as is standard in all quantum chemistry programs. "
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Working with the Inertia Tensor\n",
        "The inertia tensor is a symmetric matrix of the following shape:\n",
        "$$\n",
        "\\mathbf{I} = \\begin{pmatrix}\n",
        "I_{xx} & I_{xy} & I_{xz} \\\\\n",
        "I_{yx} & I_{yy} & I_{yz} \\\\\n",
        "I_{zx} & I_{zy} & I_{zz} \\\\\n",
        "\\end{pmatrix} \n",
        "$$\n",
        "where $I_{xx} = \\sum_{k=1}^N m_k (y_k^2 + z_k^2)$ etc. Please refer to [this article](https://en.wikipedia.org/wiki/Moment_of_inertia) for a more detailed notation of the corresponding elements. "
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Task 1 \n",
        "\n",
        "Read in the geometry of the benzene molecule given in `data/exercise_4/benzene.xyz` and shift the molecule to its center of mass. Same as in exercise 3, use `scipy.constants` for all elementary constants needed in this exercise.\n",
        "\n",
        "Compute the inertia tensor in atomic units, i.e. convert the coordinates to Bohr.\n",
        " "
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Task 2\n",
        "\n",
        "We will now build the transformation matrix $\\mathbf{D}$ that allows to separate the TR and V DOF. To ease definition of $\\mathbf{D}$, we will use the non-MW Hessian. In doing so, we avoid back-and-forth (un-)mass-weighting of the Hessian, and only need to do it after projecting out the TR modes. The transformation matrix for $\\mathbf{H}$ is of size $6 \\times (N_\\mathrm{at} \\cdot 3)$ in the case of a non-linear molecule. We need to define the basis vectors $\\mathrm{T}$ and $\\mathrm{R}$ for the T and R DOF.\n",
        "\n",
        "Think about the translation of a free atom in space and write down its translational basis vectors along the cartesian directions. Use this idea of the free atom to express $\\mathrm{T}$ for a molecule consisting of $N_\\mathrm{at}$. \n",
        "\n",
        "The definition of $\\mathrm{R}$ is slightly more involved. We need the eigenvectors $\\mathbf{X}$ of $\\mathbf{I}$ for these entries of the projector and the Center of mass coordinates $\\vec{r}$ of the atoms: \n",
        "$$\n",
        "\n",
        "(D_4)_{i} = R_x = \\vec{r}_i \\times X_3 \\\\\n",
        "(D_5)_{i} = R_y = \\vec{r}_i \\times X_2 \\\\\n",
        "(D_6)_{i} = R_z = \\vec{r}_i \\times X_1 \\\\\n",
        "$$\n",
        "where $i$ refers to the $i$-th atom and X_j refers to the $j$-th eigenvector in X. $\\times$ refers here to the cross product available via `np.cross`.  These cross-product terms are then formed for all atoms in the molecule and concatenated in a vector.\n",
        "\n",
        "Write functions to form $\\mathbf{D}$."
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {},
      "source": [
        "### Task 3\n",
        "\n",
        "Next, we will perform a QR decomposition using `numpy`. This way, a real matrix (in our case $\\mathbf{D}$) is decomposed into an orthogonal $\\mathbf{Q}$ and upper triangle matrix $\\mathbf{R}$:\n",
        "$$\n",
        "\\mathbf{A} = \\mathbf{Q} \\mathbf{R}\n",
        "$$.\n",
        "\n",
        "Compute the orthogonal projection matrix $\\mathbf{Q}$ from a QR decomposition of the transformation matrix $\\mathbf{D}$. Use the orthogonal projector (OP) to obtain the final projection matrix $\\mathbf{M}$ as:\n",
        "$$\n",
        "\\mathbf{M} = \\mathbf{\\mathcal{I}} - \\mathbf{Q} \\cdot \\mathbf{Q}^T\n",
        "$$\n",
        "where $\\mathbf{\\mathcal{I}}$ is the identity matrix.\n",
        "\n",
        "Check your result for M the projection matrix with the reference projection matrix (`proj.mat`). \n",
        "\n",
        "Compute the Hessian matrix for benzene using the STO-3G basis set and the coordinates given in benzene.xyz. Be aware this may take some computing time.\n",
        "\n",
        "Use $\\mathbf{M}$ to project out the TR DOF from the (now mass-weighted hessian!) according to the general projection equation $\\mathbf{M} \\mathbf{A} \\mathbf{M}$. Check that the first 6 frequencies are now indeed (close) to 0."
      ]
    }
  ],
  "metadata": {
    "colab": {
      "provenance": []
    },
    "kernelspec": {
      "display_name": "myvenv",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.8.10"
    },
    "orig_nbformat": 4
  },
  "nbformat": 4,
  "nbformat_minor": 0
}
