{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# COS 3a Exercise 12\n",
    "\n",
    "---\n",
    "Submission until 21/01/2025 12:00 p.m.\n",
    "\n",
    "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tutorial\n",
    "\n",
    "### Davidson Algorithm\n",
    "\n",
    "In this exercise, we will learn about a very prominent algorithm that is used in most $ab~initio$ excited-state quantum chemical methods. The so-called Davidson algorithm is a numerical procedure that allows finding some $K\\leq N$ eigenvalues of a matrix $\\mathbf{A}$ efficiently, where $\\mathbf{A}\\in \\mathbb{R}^{N\\times N}$ for example. As you might expect using these kind of approximate eigenvalue solvers over a full diagonalization of the corresponding matrices is only necessary in cases where sufficiently large matrices are considered. \n",
    "One such example is the orbital rotation hessian matrix $\\mathbf{A}$ of the Tamm-Dancoff approximation (TDA) eigenvalue problem:\n",
    "$$\n",
    "\\mathbf{A}\\mathbf{X}= \\mathbf{X}\\boldsymbol{\\omega} \n",
    "$$\n",
    "whose eigenvalues and -vectors are of interest, e.g. for the computation of spectroscopical properties. From the equation of the matrix element $A_{ia,jb}$ (see lecture):\n",
    "$$\n",
    "\\delta_{ij}\\delta_{ab}(\\epsilon_a - \\epsilon_i) + \\lbrack ia|jb \\rbrack - \\lbrack ij|ab \\rbrack, \\\\ i,j \\in n_\\mathrm{occ},~a,b\\in n_\\mathrm{virt}\n",
    "$$\n",
    "we see that $\\dim(\\mathbf{A}) = ((n_\\mathrm{occ}n_\\mathrm{virt}), (n_\\mathrm{occ}n_\\mathrm{virt}))$.\n",
    "\n",
    "Let's consider the objective of computing the absorption spectrum of Benzene by means of a TDA calculation. Below, you find the code to do the ground state calculation with `pyscf` when using only a \"small\" def2-SVP basis set:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyscf import gto, dft, tddft\n",
    "\n",
    "mol = gto.Mole()\n",
    "mol.build(\n",
    "    atom = \"data/exercise_4/benzene.xyz\",\n",
    "    basis = 'def2-SVP',\n",
    ")\n",
    "\n",
    "mf = dft.RKS(mol)\n",
    "mf.xc = 'pbe0'\n",
    "mf.kernel()\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "C = mf.mo_coeff \n",
    "occ = mf.mo_occ\n",
    "occmo = C[:,occ==2]\n",
    "virtmo = C[:,occ==0]\n",
    "nocc, nvirt = occmo.shape[-1], virtmo.shape[-1]\n",
    "print(nocc *nvirt)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "mytd = tddft.TDA(mf)\n",
    "mytd.nstates = 2\n",
    "A = mytd.get_ab()[0] # Get A matrix only"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = A.reshape(nocc * nvirt, nocc* nvirt) # Reshape 4D tensor to matrix\n",
    "print(A.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Already for this small test case, $\\mathbf{A}$ gets rather large ($\\sim2000 \\times 2000$). Constructing the full $\\mathbf{A}$ matrix is costly already, and it quickly becomes unfeasible to do a full diagonalization (scales roughly $\\mathcal{O}(N^3)$, where $N$ is a dimension of the matrix) to obtain the complete eigenvalue spectrum (here: excitation energies). \n",
    "\n",
    "More often we are interested in determining a few eigenvalues, for example when trying to simulate UV-Vis absorption spectra (restricted energy range of particular interest). The Davidson algorithm is a much more economic choice to diagonalizing $\\mathbf{A}$ completely in the sense that only a subspace of $\\mathbf{A}$ is actually diagonalized, and we can obtain some targeted lowest (or highest) $k$ eigenvalues and -vectors instead. \n",
    "\n",
    "Starting from the Rayleigh-Ritz projection formula, we can project $\\mathbf{A}$ onto a subspace spanned by a set of $m$ trial vectors $\\{\\vec{v}_i\\} = \\mathbf{V}$ (orthonormal):\n",
    "\\begin{align}\n",
    "\\mathbf{A}^\\prime = \\mathbf{V}^T \\mathbf{A} \\mathbf{V}\n",
    "\\end{align}\\tag{1}\n",
    "In the Davidson algorithm, we initialize a set $\\mathbf{V}\\in \\mathbb{R}^{N \\times m}$ where $m << N$ (m is incremented during the procedure). Say, for example, we start with two trial vectors such that $\\mathbf{V}\\in \\mathbb{R}^{N \\times 2}$. We compute $\\mathbf{A}^\\prime$ using eq. 1. \n",
    "\n",
    "Next, we diagonalize $\\mathbf{A}^\\prime$ ($\\dim(\\mathbf{A}^\\prime) = m \\times m$) and obtain eigenvectors $\\vec{X}_i$ and eigenvalues $\\omega^\\prime_i$. The eigenvalue equation reads:\n",
    "$$\n",
    "\\mathbf{A}^\\prime \\vec{X}^\\prime_i = \\vec{X}^\\prime_i \\omega^\\prime_i \\,.\n",
    "$$\n",
    "These eigenvectors will then be used to update the trial vector space. We compute the residuals \n",
    "$$\\vec{r}_i = \\mathbf{A}\\mathbf{V}\\mathbf{X}^\\prime - \\omega_i^\\prime\\mathbf{V}\\mathbf{X}^\\prime_i\n",
    "$$ \n",
    "from $\\mathbf{X}_i^\\prime$. These are zero if we find the \"true\" eigenvalues of $\\mathbf{A}$. We use these to determine convergence, since they should be below some threshold $\\tau$ such that the approximated eigenvalues of $\\mathbf{A}$ are reasonably accurate. If we did not converge within this iteration (subspace spanned by $\\mathbf{V}$ does not approximate the eigenvectors sufficiently), we add the residuals $\\vec{r}_i$ to our subspace: $\\mathbf{V}= \\lbrack \\{\\mathbf{v_i}\\}\\cup\\{\\vec{r}_i\\} \\rbrack$ and continue. More (mathematical) details can be found [here](https://gqcg-res.github.io/knowdes/the-davidson-diagonalization-method.html) and [here](https://joshuagoings.com/2013/08/23/davidsons-method/) in case you are interested. \n",
    " \n",
    "For the sake of this exercise, we will use the Davidson algorithm mainly for demonstration purposes on how to deal with CIS-like excited-state eigenvalue problems. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Imports the custom implementation of a Davidson solver\n",
    "from davidson import Davidson\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import time"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1\n",
    "\n",
    "Write a function to determine the \"sparsity\" and \"density\" of a matrix $\\mathbf{A}$. We define sparsity and density $\\mathcal{D}$ of a matrix $\\mathbf{M}$ as:\n",
    "$$\n",
    "\\mathcal{S} = 1 - \\mathcal{D} = 1 - \\frac{N}{n\\cdot m}\n",
    "$$\n",
    "where $N$ is the number of non-zero matrix elements and $\\dim(\\mathbf{A}) =(n,m)$. Include an optional argument in your function that recognizes numerical \"near-zero\" cases correctly.\n",
    "\n",
    "Implement a function that builds diagonally-dominant, i.e. largest contributions on the diagonal, random symmetric matrices (use `np.random.randn`) with varying sparsity/density. \n",
    "\n",
    "Investigate the time needed to do a full diagonalization (`np.linalg.eigh`) against that of the provided Davidson solver (`davidson.Davidson`) for matrices of different sparsity. Summarize your results in a barplot giving the matrix dimension and CPU time needed. \n",
    "\n",
    "Briefly comment for which types of matrices you expect the Davidson solver to pay-off more in regard to the timing (sparse or dense matrices). Give an explanation why you think this is the case. Also explain whether this generally true or just for our implementation? What \"inputs\" affect the convergence behaviour of our Davidson solver? \n",
    "Explain in your own words what the residual equation tells you about the Davidson and what is it good for (except for the convergence criterion).  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2 \n",
    "\n",
    "Take the above example of Benzene (PBE0/def2-SVP) and compute the first 10 excited states using TDA with `pyscf`. Use \n",
    "1) the built-in solver of `pyscf`\n",
    "2) the provided custom Davidson implementation \n",
    "3) a full diagonalization of $\\mathbf{A}$ \n",
    "\n",
    "to determine the excitation energies. Compare the timings and discuss the results. \n",
    "How do you expect the order of fastest--lowest solver changes for larger systems? Briefly explain your thoughts.\n",
    "\n",
    "<u>Opt:</u> If the execution with def2-SVP takes too long, you can also choose a slightly smaller basis set like 6-31G* for testing purposes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "exvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
