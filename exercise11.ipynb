{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 11\n",
    "\n",
    "---\n",
    "Submission until 14/01/2025 12:00 p.m.\n",
    "\n",
    "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pyscf import gto, scf, ao2mo\n",
    "import numpy as np\n",
    "import time\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Electronic Integrals\n",
    "\n",
    "Throughout this exercise, we will adapt a notation for orbitals that is also frequently used in the literature:\n",
    "- $\\mu,\\nu, \\lambda, \\kappa$: Basis functions/ AO indices\n",
    "- $i,j,k,l$: Occupied MO indices\n",
    "- $a,b,c,d$: Virtual (unoccupied) MO indices\n",
    "- $p,q,r,s$: \"General\" MO indices, i.e. either virtual or occupied  \n",
    "\n",
    "Furthermore, recapture the Mulliken notation to declare electronic integrals\n",
    "$$\n",
    "(\\mu\\nu|\\lambda\\kappa) = \\int\\int \\chi_\\mu(\\mathbf{r}_1)  \\chi_\\nu(\\mathbf{r}_1) \\chi_\\lambda(\\mathbf{r}_2)  \\chi_\\kappa(\\mathbf{r}_2) d\\mathbf{r}_1 d\\mathbf{r}_2\n",
    "$$\n",
    "for example. \n",
    "The above-defined electron repulsion integrals (ERIs) are six-dimensional integrals (electron coordinates in 3D $\\mathbf{r}_i$, usually not easy to evalute analytically). One particular reason why almost all (molecular) quantum chemistry codes work with Gaussian-type orbitals (GTOs) is the fact that these can be decomposed into 1D integrals as a consequence of the Gaussian product theorem (GPT). We will get back to ERIs in Task 4.\n",
    "\n",
    "There exists broad literature on the general topic of molecular integrals of GTOs (and their efficient evaluation). For the sake of this exercise, we keep the introduction somewhat concise to the transformations we are concerned with.\n",
    "\n",
    "An extensive overview of this topic is given in the book Molecular Electronic-Structure theory of Helgaker Jørgensen and Olsen. You can also find some words on integrals in the book Modern Quantum Chemistry by Szabo and Ostlund. The former introduces a slightly more compact notation of what the Mulliken notation offers in the sense that it helps us in defining property integrals more concise:\n",
    "$$\n",
    "O_{\\mu\\nu} = (\\mu|\\hat{O}_1|\\nu) = \\int \\chi_\\mu(\\mathbf{r}) |\\hat{O}(\\mathbf{r})| \\chi_\\nu(\\mathbf{r})d\\mathbf{r} \n",
    "$$\n",
    "for 1-electron-2-center (1e-2c) integrals (3D) or\n",
    "$$\n",
    "O_{\\mu\\nu\\lambda\\kappa} = (\\mu\\nu|\\hat{O}_{12}|\\lambda\\kappa) =  \\int\\int \\chi_\\mu(\\mathbf{r}_1)  \\chi_\\nu(\\mathbf{r}_1) |\\hat{O}_{12}| \\chi_\\lambda(\\mathbf{r}_2)  \\chi_\\kappa(\\mathbf{r}_2) d\\mathbf{r}_1 d\\mathbf{r}_2\n",
    "$$\n",
    "for 2e-4c integrals. $\\hat{O}$ is an arbitrary operator, e.g. the positon operator $\\hat{\\mathbf{r}}$ (see below).\n",
    "\n",
    "### MO coefficients and AO integrals\n",
    "Below, you find a minimum working example on how to extract the MO coefficient matrix and other relevant quantities from `pyscf`. You will need those to do integral transformations. In priniple, you can store the calculated electronic integrals on disk and reloaded them for later calculations, in order to avoid recomputing costly integrals again. `pyscf` offers some functionality [here](https://pyscf.org/contributor/ao2mo_developer.html), in case you are interest in that. This data can become very large, which is the reason why 2e-integrals for large systems are usually not stored in this \"full\" form. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Nbf: 10\n",
      "converged SCF energy = -105.348247521975\n",
      "Dimensions of the MO coefficient matrix: (10, 10)\n",
      "               #0        #1        #2        #3        #4       \n",
      "0 Li 1s          0.00032   0.99025  -0.05621   0.10142  -0.00000\n",
      "0 Li 2s         -0.00313   0.03407   0.05569  -0.23314   0.00000\n",
      "0 Li 2px         0.00000  -0.00000  -0.00000   0.00000  -0.00231\n",
      "0 Li 2py        -0.00000  -0.00000  -0.00000   0.00000   0.29332\n",
      "0 Li 2pz        -0.00442  -0.00432   0.05948  -0.20092   0.00000\n",
      "1 F 1s           0.99470  -0.00478  -0.25821  -0.03295   0.00000\n",
      "1 F 2s           0.02258   0.01519   0.98792   0.19362  -0.00000\n",
      "1 F 2px          0.00000   0.00000  -0.00000   0.00000  -0.00725\n",
      "1 F 2py          0.00000   0.00000  -0.00000   0.00000   0.92268\n",
      "1 F 2pz         -0.00135  -0.01139  -0.02170   0.91703  -0.00000\n",
      "               #5        #6        #7        #8        #9       \n",
      "0 Li 1s         -0.00000  -0.23887   0.00000  -0.00000  -0.11493\n",
      "0 Li 2s          0.00000   0.88998  -0.00000   0.00000   0.56188\n",
      "0 Li 2px         0.29332  -0.00000   0.04770   0.96189  -0.00000\n",
      "0 Li 2py         0.00231   0.00000   0.96189  -0.04770  -0.00000\n",
      "0 Li 2pz         0.00000  -0.49459   0.00000   0.00000   0.96960\n",
      "1 F 1s           0.00000   0.00975   0.00000   0.00000   0.09001\n",
      "1 F 2s          -0.00000  -0.04113  -0.00000  -0.00000  -0.59483\n",
      "1 F 2px          0.92268   0.00000  -0.01994  -0.40219   0.00000\n",
      "1 F 2py          0.00725  -0.00000  -0.40219   0.01994   0.00000\n",
      "1 F 2pz         -0.00000   0.11280   0.00000   0.00000   0.42326\n",
      "MO occupation vector: [2. 2. 2. 2. 2. 2. 0. 0. 0. 0.]\n",
      "Dimension of the 1e-2c AO ints: (3, 10, 10)\n"
     ]
    }
   ],
   "source": [
    "# https://pyscf.org/contributor/ao2mo_developer.html\n",
    "# RHF-STO-3G calculation of H2 \n",
    "from pyscf.tools import dump_mat\n",
    "\n",
    "# Build pyscf mol and show some relevant quantitites for the integral transformations\n",
    "mol = gto.Mole()\n",
    "mol.build(\n",
    "    #atom = \"H 0 0 0; H 0 0 1\",  # in Angstrom\n",
    "    #atom = \"data/exercise_11/ch4.xyz\",\n",
    "    atom = \"Li 0 0 0; F 0 0 1.67\",\n",
    "    basis = \"STO-3G\",\n",
    "    #basis = \"def2-SVP\",\n",
    ")\n",
    "\n",
    "# Note: nbas is not the number of basis functions but the number of shells, see also: https://github.com/pyscf/pyscf.github.io/blob/master/examples/gto/11-basis_info.py\n",
    "\n",
    "print(f\"Nbf: {mol.nao_nr()}\")\n",
    "\n",
    "\n",
    "# Converged SCF obj that contains MO coeffs.\n",
    "mf = scf.RHF(mol)\n",
    "mf.kernel()\n",
    "\n",
    "C = mf.mo_coeff\n",
    "print(f\"Dimensions of the MO coefficient matrix: {C.shape}\")\n",
    "dump_mat.dump_mo(mol, mf.mo_coeff) # Prints some info on the basis functions and displays the coeff matrix \n",
    "print(f\"MO occupation vector: {mf.mo_occ}\")\n",
    "rmunu = mol.intor('int1e_r') # 1e-2c integrals (mu | r | nu) - r_munu\n",
    "print(f\"Dimension of the 1e-2c AO ints: {rmunu.shape}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "From this example we see that the rows correspond to the AO indices and the columns to the MO indices."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1\n",
    "\n",
    "From the single line `rmunu = mol.intor('int1e_r')`, we can already identify a very handy feature of pyscf: We have serveral molecular integrals at hand quiet easily. `pyscf` uses the integral library `libcint` in the background, which is a big reason for its comparably good performance as a python program compared to low-level programming languages like Fortran, C++. Further information on the programm structure of pyscf and further references to what libcint is capable of can be found [here](https://wires.onlinelibrary.wiley.com/doi/epdf/10.1002/wcms.1340) and [here](http://dx.doi.org/10.1002/jcc.23981).\n",
    "\n",
    "Using the above nomenclature, we are interest in the dipole integrals $r_{\\mu\\nu}$ that we can use to compute the dipole moment of our molecule. We choose the example of LiF with $d = 1.67~\\mathrm{\\AA}$., which has a non-vanishing dipole moment $\\vec{\\mu}$.\n",
    "\n",
    "\n",
    "The $x$-component of the dipole moment $\\mu_x$ can be written as:\n",
    "\\begin{align}\n",
    "\\mu_x = -\\sum_\\mu \\sum_\\nu P_{\\mu\\nu} (\\mu |\\hat{r}_x|\\nu )+ Q_A R_{x,A}\n",
    "\\end{align}\\tag{2}\n",
    "where $P_{\\mu\\nu} = \\sum_i^{N_\\mathrm{occ}} f_i C_{\\mu i} C_{i\\nu}$ is the 1-particle density matrix (occupation number $f_i = 2$), $Q_A$ the nuclear charges and $R_A$ the cartesian positions of atom $A$.\n",
    "\n",
    "- Write a function to compute $P$ using either explicit for loops or matrix multiplication.\n",
    "\n",
    "- Write a function to compute the dipole moment of LiF (in atomic units) and check your result against pyscf (`mf.dip_moment(unit=\"au\"))`). \n",
    "\n",
    "<u> Obs:</u> $r_{\\mu\\nu}$ is an origin-dependent property. To achieve a consistent description, molecules are usually shifted to the center of mass (or charge). Since we do not touch the default orientation of the molecule in cartesian space, we get the same integrals as the built-in function in `pyscf`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get number of occ. orbitals for comp. of P\n",
    "nocc = mol.atom_charges().sum() // 2\n",
    "# Nuclear charges and coordinates \n",
    "charges = mol.atom_charges()\n",
    "coords  = mol.atom_coords()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 2\n",
    "\n",
    "Knowing that we can get our hands on property integrals like $\\mathbf{r}_{\\mu\\nu}$ easily, now leads us to the point that we want to apply basis transformations to these 1e-quantities. We can express the 1e-integral transformation as:\n",
    "$$\n",
    "O_{pq} = \\sum_{\\mu\\nu} C_{\\mu p}^* O_{\\mu\\nu} C_{\\nu q} \n",
    "$$\n",
    "or $\\mathbf{O}^\\mathrm{MO} = \\mathbf{C}^\\dagger \\mathbf{O}^\\mathrm{AO} \\mathbf{C}$ as a matrix product.\n",
    "\n",
    "- Implement a function that allows you to transform 1e-integrals from the AO to the MO basis. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 3\n",
    "\n",
    "Since we now have learned about property calculations for the electronic ground state, let's recapture the contents of exercise 8. There, we learned about optical spectra, in particular, electronic circular dichroism (ECD) spectroscopy.\n",
    "The quantity proportional to the \"intensity\" in ECD spectroscopy is the rotor strength $R_{0m}$, that is associated with the transition from state $0$ (the electronic ground state) to state $m$, an excited state.\n",
    "$$\n",
    "R_{nm} = -\\mathrm{Im}(\\vec{\\mu}_{0m} \\cdot \\vec{m}_{m0})\n",
    "$$\n",
    "Hence, in the case of ECD, after calculating the transition electric dipole $\\vec{\\mu}_{0m}$ and the transition magnetic dipole $\\vec{m}_{m0}$ moments, the computation of spectral intensities boils down to a simple scalar product between these vectors. \n",
    "\n",
    "For the sake of simplicity, we focus on absorption spectra for this exercise. The reason being that the oscillator strength $f_{0m}$ is the quantity proportional to the absorption intensity:\n",
    "$$\n",
    "f_{0m} = \\frac{2}{3}  \\epsilon_{0m} \\vec{\\mu}_{0m}\\vec{\\mu}_{m0}\n",
    "$$\n",
    "which is easier computed since we only need $\\vec{\\mu}_{0m}$. Additionally, $\\vec{\\mu}_{0m}$ is symmetric ($\\vec{\\mu}_{m0} = \\vec{\\mu}_{0m}$), so we only need the *transition* dipole moment between these states and can compute the absorption spectrum rightaway. \n",
    " \n",
    "To compute the electric transition dipole moment $\\vec{\\mu}_{m0}$, we use the dipole integrals $r_{\\mu\\nu}$ and transform these to the MO basis first. Next, we need to \"contract\" these dipoles with the excited state eigenvectors. In the Tamm-Dancoff approximation (TDA), the eigenvalue problem:\n",
    "$$\n",
    "\\mathbf{AX} =  \\mathbf{X}\\boldsymbol \\omega\n",
    "$$\n",
    "is solved. Again, we use the easier problem (TDA vs TDDFT) and do not care much about different representations of $\\vec{\\mu}_{m0}$ (more on that in the lecture). The property calculation is still perfectly analogue to the case of TD-DFT with ECD, though simpler equations are involved.\n",
    "\n",
    "Below you find an example on how to perform a TDA-PBE0/def2-SVP calculation of LiF with pyscf. \n",
    "- Figure out how to compute the electric transition dipole moment of the `tdobj` (named `mytd` below) and the oscillator strength. Use these values as reference for our own computations.\n",
    "- Use the equation for the AO $\\rightarrow$ MO transformation in Task 2 to compute transition dipole integrals in the MO basis. To do so, you need to compute $(i|\\hat{\\mathbf{r}}|a) =  \\sum_{\\mu} \\sum_\\nu C_{\\mu i} \\mathbf{r}_{{\\mu\\nu}} C_{\\nu a} $.\n",
    "- Use these integrals and build the electric transition dipole moments, using the eigenvectors from your TDA calculation (`mytd.xy`): \n",
    "$\\vec{\\mu}_{0m} = \\sum_i \\sum_a 2 (i|\\hat{\\mathbf{r}}|a) \\cdot X_{0m,ia}$\n",
    "\n",
    "<u> Hint:</u> The dimension of $\\mathbf{X}$ in the TDA calculation is a bit difficult to understand first. It is a list with length `nstates`. In each entry, there is a tuple of the eigenvector matrices $\\mathbf{X}$ and $\\mathbf{Y}$, where the latter is always zero (results from Tamm-Dancoff approximation). You will only need to use the first entry of these tuples - the actual $\\mathbf{X}$, dim: (nocc, nvirt)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "converged SCF energy = -105.709073952619\n",
      "Excited State energies (eV)\n",
      "[3.33901549 3.33901549]\n"
     ]
    }
   ],
   "source": [
    "from pyscf import dft, tddft\n",
    "\n",
    "mol = gto.Mole()\n",
    "mol.build(\n",
    "    #atom = \"H 0 0 0; H 0 0 1\",  # in Angstrom\n",
    "    #atom = \"data/exercise_11/ch4.xyz\",\n",
    "    atom = \"Li 0 0 0; F 0 0 1.67\",\n",
    "    basis = 'STO-3G',\n",
    ")\n",
    "\n",
    "mf = dft.RKS(mol)\n",
    "mf.xc = 'pbe0'\n",
    "mf.kernel()\n",
    "\n",
    "mytd = tddft.TDA(mf)\n",
    "mytd.nstates = 2\n",
    "_= mytd.kernel()\n",
    "#mytd.analyze()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "57vzlqbm2RBL"
   },
   "source": [
    "## Task 4\n",
    "\n",
    "As a final task, we want to take a look at the ERIs and again the basis transformation from the AO basis to the MO basis. We use the MO coefficients obtained from our SCF calculation once again. The AO $\\rightarrow$ MO transformation, if coded naively, *essentially* scales as $O(N^8)$ where $N$ is the number of basis functions.\n",
    "The transformation can be expressed like this in case of general orbitals:\n",
    "\\begin{align}\n",
    "(pq|rs) = \\sum_\\mu \\sum_\\nu \\sum_\\lambda \\sum_\\kappa C_{\\mu p} C_{\\nu q} C_{\\lambda r} C_{\\kappa s} (\\mu\\nu|\\lambda\\kappa)\n",
    "\\end{align}\n",
    "\n",
    "Use the above information to write your own function that performs the AO $\\rightarrow$ MO transformation of the ERIs. Compare your implementation with the pyscf analogue transformation routine (`ao2mo`) and comment on the severe performance differences found for the example of methane (`data/exercise_11/ch4.xyz`) using again two basis sets.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "exvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
