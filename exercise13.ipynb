{
  "cells": [
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "PnaAVSzkoO8O"
      },
      "source": [
        "# COS 3a Exercise 13\n",
        "\n",
        "---\n",
        "Submission until 28/01/2025 12:00 p.m."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 16,
      "metadata": {
        "id": "AUox5qP6oO8Q"
      },
      "outputs": [],
      "source": [
        "from scipy.interpolate import CubicSpline\n",
        "import numpy as np\n",
        "import matplotlib.pyplot as plt"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "SzSrrcxOoO8R"
      },
      "source": [
        "# Fourier Transform on EXAFS data analysis\n"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "nJj5KNHRoO8S"
      },
      "source": [
        "## Task 1 \n",
        "\n",
        "In this exercise, we will use the Fast Fourier Transformation (FFT, see also exercise 10) on an experimental EXAFS signal of Ferrocene to determine the Fe-C distances and compare them to a given theoretical reference distance. More on the topic of EXAFS in general will follow in this weeks lecture! If you need information to work on the provided tasks, [this source](https://chem.libretexts.org/Bookshelves/Physical_and_Theoretical_Chemistry_Textbook_Maps/Supplemental_Modules_(Physical_and_Theoretical_Chemistry)/Spectroscopy/X-ray_Spectroscopy/EXAFS_-_Theory) provides some relevant data on the EXAFS equation and the theory behind this spectroscopy.\n",
        "\n",
        "The theoretical estimates have been computed using density functional theory. The experimental data (`exafs.dat`) is an EXAFS spectrum of the Fe K edge. The datapoints are the $k^3$-weighted EXAFS signal, $\\chi(k) \\cdot k^3$, and the corresponding wave vectors $k$ (see the EXAFS equation (2) below).\n",
        "\n",
        "Experimental reference: $d_\\mathrm{Fe-C} = 2.04~\\mathrm{\\AA}$\n",
        "\n",
        "Theoretical (DFT) reference: $d_\\mathrm{Fe-C} = 2.05~\\mathrm{\\AA}$\n",
        "\n",
        "As a first step, it is necessary to transform the EXAFS spectra into the frequency domain. To perform the Fourier transformation of the signal, you can use numpy’s inverse FFT function (`np.fft.ifft()`). \n",
        "(It does not matter here if we use the inverse FFT or FFT).\n",
        "\n",
        "After transforming the EXAFS spectra, you can determine the distance $R$ from the peak maximum of the highest peak in the corresponding radial distribution function (RDF).\n",
        "\n",
        "To convert from the discrete indexed points to distances, use the following formula:\n",
        "\n",
        "\\begin{equation}\n",
        "R_n=n \\cdot \\Delta R=n \\cdot \\frac{\\pi}{N \\cdot \\Delta k}\n",
        "\\end{equation}\n",
        "\n",
        "$n$ is the index of the inverse FFT element $x_n$ and $\\Delta k$ is the step size in the X-ray photon wavenumber.\n",
        "\n",
        "Since this approach only works with evenly spaced data points, you have to interpolate the data points first.\n",
        "\n",
        "---\n",
        "\n",
        "#### Tasks\n",
        "\n",
        "1.1 Visualize the discrete and interpolated data points using [cubic spline interpolation](https://docs.scipy.org/doc/scipy/reference/generated/scipy.interpolate.CubicSpline.html) from scipy. \n",
        "\n",
        "- Hint: To perform a cubic spline interpolation, you can first define a cubic spline object with the given data points and then evaluate the function at evenly spaced x values. Use the boundary condition `bc_type='natural'`.\n",
        "    ```python\n",
        "    cs = CubicSpline(x, y, bc_type='natural')   # x and y are the given data points\n",
        "    y_new = cs(x_new)                           # x_new are the evenly spaced x values which you have to define yourself\n",
        "    ```\n",
        "\n",
        "1.2 Perform an FFT of the spectrum and visualize the resulting peak spectrum.\n",
        "\n",
        "1.3 Determine the Fe-C distance from the peak maximum of the highest peak in the RDF.\n",
        "- Hint: Since the spectrum is symmetric, you can use the first significant part of the spectrum to determine the peak maximum. Think about a suitable threshold by taking the given values of the Fe-C distance into account.\n",
        "\n",
        "1.4 As you will notice, there is a clear discrepancy between the computed and the reference values of $d_\\mathrm{Fe-C}$. One reason for this large deviation is that the EXAFS signal we used in the FFT was not corrected for the scattering phase-shift, i.e., the $\\delta_j (k) $ term in the EXAFS equation. Briefly comment on how this phase-shift might influence the computed distance. \n"
      ]
    },
    {
      "cell_type": "code",
      "execution_count": 17,
      "metadata": {},
      "outputs": [],
      "source": [
        "# Write your code here"
      ]
    },
    {
      "cell_type": "markdown",
      "metadata": {
        "id": "cw3P91rvoO8V"
      },
      "source": [
        "## Task 2\n",
        "\n",
        "Now we want to derive the shift observed for the experimentally obtained distances $R_j$, considering a phase-shift of $\\delta_j(k) \\approx -k$. To do so, we need to simulate the $k^3$-weighted EXAFS, i.e., $k^3\\cdot\\chi(k)$, using the experimental reference distance of $2.04~\\mathrm{\\AA}$ for Fe-C. Use $\\sigma_j = 0.1~~\\mathrm{\\AA}^{-1}$ and $f_j(k) = \\frac{1}{40}$.\n",
        "\n",
        "The EXAFS equation, where we need to introduce these values reads:\n",
        "\\begin{equation}\n",
        "\\chi(k)=\\sum_j \\frac{N_j f_j(k) \\mathrm{e}^{-2 k^2 \\sigma_j^2}}{k R_j^2} \\sin \\left[2 k R_j+\\delta_j(k)\\right]\n",
        "\\end{equation}\\tag{2}\n",
        "\n",
        "Here, you have to set the number of equivalent atoms in the first shell. For simplicity, it is assumed that hydrogens do not contribute to the scattering.\n",
        "\n",
        "---\n",
        "### Tasks\n",
        "2.1 Generate the data for the EXAFS spectrum with no phase-shift and with a phase-shift of $\\delta_j(k) = - k$.\n",
        "\n",
        "2.2 Plot these spectra along with the original experimental signal for $k^3\\cdot\\chi(k)$ in one graph.\n",
        "\n",
        "2.3 Briefly describe whether the EXAFS model is now able to reproduce the experimental spectrum with the correct distance."
      ]
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {},
      "outputs": [],
      "source": [
        "# Write your code here"
      ]
    }
  ],
  "metadata": {
    "colab": {
      "provenance": []
    },
    "kernelspec": {
      "display_name": "venv",
      "language": "python",
      "name": "python3"
    },
    "language_info": {
      "codemirror_mode": {
        "name": "ipython",
        "version": 3
      },
      "file_extension": ".py",
      "mimetype": "text/x-python",
      "name": "python",
      "nbconvert_exporter": "python",
      "pygments_lexer": "ipython3",
      "version": "3.12.0"
    },
    "orig_nbformat": 4
  },
  "nbformat": 4,
  "nbformat_minor": 0
}
