{
 "cells": [
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# COS 3a Exercise 2\n",
    "---\n",
    "Submission by 22/10/2024 12:00 p.m\n",
    "\n",
    "## Tutorial\n",
    "\n",
    "### Lists\n",
    "\n",
    "When we want to store multiple data that should be accessed by an index, we can use a list in Python.  \n",
    "The syntax to create an empty list is as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 44,
   "metadata": {},
   "outputs": [],
   "source": [
    "empty_list = []"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "In Python lists are very flexible programming objects, for the following reasons:\n",
    "\n",
    "* Values of any type can be stored in a list and can also be mixed.\n",
    "* Values can be appended to lists at any point.\n",
    "* Values can be accessed by an iterator or by index.\n",
    "* The number of elements in the list can be determined by the built-in function `len()`\n",
    "\n",
    "Lists are a builtin object type in Python, meaning there are no extensions or packages needed to use them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#initialize a list with mixed data type members\n",
    "mixed_list = [1, 2.567, True, \"Hello\"]\n",
    "#append a value to the list\n",
    "mixed_list.append(\"World\")\n",
    "print(mixed_list)\n",
    "#iterate using iterator\n",
    "for value in mixed_list:\n",
    "    print(value, type(value))\n",
    "#iterate using index\n",
    "#we use the len() function to get the number of elements in mixed_list\n",
    "for index in range(0, len(mixed_list)):\n",
    "    print(mixed_list[index])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the last loop, we used an index to access the elements of the list. As we can see the elements are stored in the same order as they have been defined or been appended.  \n",
    "It is important to see that Python uses 0-indexing instead of 1-indexing (as in Fortran for example).  \n",
    "This means that the first element of the list is accessed by the index `0` and not `1`.  \n",
    "To get access to the value of the element we use the index operator `[ ]`, this way we can use the value of the element in an operation or modify it's value.   \n",
    "A nice feature in Python is negative indexing. So you can access the last value of a list with `-1`. Correspondingly, you get the penultimate value with `-2`.\n",
    "\n",
    "Run the code in the boxes below to see how it works:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(mixed_list[0])\n",
    "print(mixed_list[1])\n",
    "#use the first and second element in a numerical operation\n",
    "print(mixed_list[0]+mixed_list[1])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed_list[0] += 1\n",
    "mixed_list[-1] += \"!\"\n",
    "print(mixed_list[0])\n",
    "print(mixed_list[-1])"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are situations where it is necessary to iterate over both the index and the value of an element, in these situations we can use the built-in function `enumerate()`.  \n",
    "`enumerate()`is a function that returns two variables the index and the element hence our for loop is changed to `for index, element`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for i, element in enumerate(mixed_list):\n",
    "    print('index:',i,'for the element:', element)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Numpy arrays\n",
    "\n",
    "Usually we want to describe our problems in quantum chemistry and in other areas in terms of linear algebra operations and objects.  \n",
    "These operations can be executed very efficiently on modern computers and offer a very concise way of coding.  \n",
    "The main objects we use in linear algebra are: **vectors**, **matrices** and higher-order **tensors**.  \n",
    "In Python these objects are introduced as numpy arrays. These arrays can have multiple dimensions as shown below:\n",
    "\n",
    "<img src=\"./images/exercise_2/numpy_arrays.png\" alt=\"np tensors\" width=\"50%\">\n",
    "\n",
    "The important parameter to determine the dimension of numpy array is the **shape** as shown above:  \n",
    "* `shape(5,)` will result in a vector with length 5  \n",
    "* `shape(2,3)` will result in a matrix with 2 rows and 3 columns\n",
    "* can be extended to more dimensions, but that is rarely used.  \n",
    "\n",
    "A few more details on numpy arrays, before we learn how to create them:\n",
    "* the elements of numpy arrays are uniform in type, mixed arrays as seen above for lists are not possible\n",
    "* the shape and therefore size of arrays remains constant, appending is not possible on the same object"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### How to create numpy arrays\n",
    "\n",
    "A lot of numpy functions return numpy arrays, one example would be `numpy.arange()`, it returns a 1D-array with evenly spaced values from the half-open interval `[start, stop)`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "\n",
    "np.arange(start=1, stop=6)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An important function or method in numpy to manipulate arrays is, `.reshape(shape=())`. We can see how it works by starting from `np.arange()` giving us a 1D-array and transforming it to 2D-array/matrix:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "m = np.arange(1,7)\n",
    "print(m)\n",
    "#reshape will reshape the array and return the reshaped copy, we need to set this returned value to m again to change our object\n",
    "m = m.reshape((2,3))\n",
    "print(m)\n",
    "#in the following line we print the returned reshaped array but m is not changed!\n",
    "print(m.reshape(3,2))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other methods to produce numpy arrays are for example `np.ones(shape)` or `np.zeros(shape)`, here the desired shape is passed as argument to the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(np.zeros(shape=(3,3)))\n",
    "print(np.ones(shape=(3,)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "numpy offers several other functions to manipulate and [construct matrices](https://numpy.org/devdocs/reference/routines.array-creation.html). Some further simple examples are `np.eye(N)` or `np.diag(v)` to generate an identity matrix of size $N$ or [construct diagonal matrices](https://numpy.org/doc/2.0/reference/generated/numpy.diag.html) from an array $v$ respectively."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# 3x3 identity matrix\n",
    "print(np.eye(3))\n",
    "\n",
    "# Diagonal matrix with 1,2,3 on diagonal\n",
    "print(np.diag([1,2,3]))"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also create arrays from the previously shown lists:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "list_ = [1, 2, 3, 4, 5, 6]\n",
    "array_ = np.array(list_).reshape(2,3)\n",
    "print(array_)"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Indexing with numpy arrays\n",
    "\n",
    "Similar to lists, we can access the elements of a numpy array using the `[ ]`operator.  \n",
    "For vectors, the indexing works just the same as for lists.  \n",
    "For ND-arrays, we can access the elements by giving their index in every dimension, i.e. `[1 , 2, ..., n]`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(array_[1,2])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first index declared in `[]` denotes the **column**, the second index the **row** of the matrix. "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another important concept when working with arrays independent of their dimension is \"slicing\".  \n",
    "We use slicing to get access to a number of elements in an array.   \n",
    "To slice an array we use the `[ ]` operator in combination with `:`, with that we can define an interval as `start:end`, which corresponds to the half-open interval `[start, end)`.  \n",
    "To illustrate here are some examples: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#for our list\n",
    "print(list_[1:3])\n",
    "#same works for arrays, with teh interval 0:1 I get access to only the element 0 in that dimension as it is half-open\n",
    "print(array_[0:1,2])\n",
    "#if we want to get the first column of our array_\n",
    "#by not giving start and end variable, start will be 0 and end the number of elements in that dimension corresponding to all values in that dimension\n",
    "print(array_[:,0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#we can use slicing to manipulate data as well\n",
    "array_[:,0] = 1\n",
    "print(array_) "
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Functions for Arrays\n",
    "\n",
    "The array object has a lot of methods that can be used with it, to get some information on your arrays or get some values.\n",
    "\n",
    "- `array.size` Gives the total amount of elements stored in an array\n",
    "- `array.shape` Returns the shape of the array\n",
    "- `array.sum()` the sum of all entries in an array is calculated\n",
    "- `array.mean()` the mean of all entries is calculated\n",
    "- `array.std()` the standard deviation of the entries is calculated\n",
    "\n",
    "The first two functions are not functions but attributes of the array, which means they are variables used to characterize the array object and are stored within its class. These are however details going beyond the scope of this class."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 1\n",
    "\n",
    "For this task, we are focussing on creating arrays and using the methods and attributes of them.\n",
    "\n",
    "Create a function that takes two integers as parameters `(nrows, ncols)`, the number of rows and columns of you matrix.  \n",
    "Within the function create two arrays of the shape defined by the parameters, one containing the values from `1` till the `nrows*ncols` spaced by 1 the other with only `0` entries.  \n",
    "Take the sum of both arrays and store it in another array. Now check that the result array and the non-zero array are equal by iterating over their entries.  \n",
    "Thereby you are proving that the zero matrix is the neutral element for matrix addition."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#put in your solution here\n",
    "def prove_zero_matrix_neutral(nrows, ncols):\n",
    "    #your solution here"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Linear algebra operations \n",
    "\n",
    "The vectors and matrices that we learned to create in the first part of this exercise are combined with linear algebra operations to perform various tasks in chemistry and other sciences.  \n",
    "In fact, linear algebra operations function as the backbone of most modern computer programs and use highly-optimized algorithms.  \n",
    "Numpy gives access to those operations and brings the speed of high-level programming languages like C, and FORTRAN to Python.  \n",
    "To quickly test this we are implementing a matrix-matrix multiplication ourself and test it against the numpy implementation.\n",
    "\n",
    "Matrix-matrix multiplications can be performed between two Matrices of sizes (l,m) and (m, n) and gives an (l,n) matrix, as seen below:\n",
    "\n",
    "<img src=\"./images/exercise_2/Matrix_multiplication_qtl1.png\" alt=\"WikiPedia\" width=\"50%\">\n",
    "\n",
    "In sum notation the element $c_{ij}$ is computed as follows:\n",
    "\n",
    "$$\n",
    "c_{ij} = \\sum_{k=0}^{n-1} a_{ik} * b_{kj}\n",
    "$$\n",
    "\n",
    "Looping over the $i,j$ indices of matrix $\\mathbf{C}$ will therefore give result to three nested for loops."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 2\n",
    "\n",
    "Implement the matrix-matrix multiplication between two given matrices A and B using three for loops and using `np.matmul` an alternative is using the `@` operator. Use one Code cell per implementation to also compare runtimes. Compare both results in terms of the matrix C that is returned and the time needed for execution. \n",
    "\n",
    "Write down a short explanation for the observed run times you just implemented."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = np.arange(0,12).reshape((4,3))\n",
    "B = np.zeros((3,3))\n",
    "for i in range(B.shape[0]):\n",
    "    B[i, i] = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#naive 3 for loop implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "#using np.matmul() store in C, you need to define C beforehand"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Visualization of Data\n",
    "\n",
    "One reason for the popularity of Python is the ease with which data can be analyzed and visualized.  \n",
    "If you have already worked with Matlab, many things will look familiar to you.  \n",
    "The module that is required is `matplotlib.pyplot` ([Documentation](https://matplotlib.org/3.5.3/api/_as_gen/matplotlib.pyplot.html)).  \n",
    "Here the module is imported and given the new name `plt` with the keyword `as`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plot x,y data series\n",
    "\n",
    "The `plot` or `scatter` function can be used to visualize two-dimensional data with the `matplotlib.pyplot` module. This takes the x-values as the first argument and the y-values as the second argument in the form as arrays or lists. **Note that the list length of the x and y values must be the same.** Many theme settings are also available. The easiest way is to pass a *format string* as the third argument, which you can compose like this:\n",
    "\n",
    "- Markers\n",
    "     - ``.'' period\n",
    "     - `'o'' circle\n",
    "     - `'*'` star\n",
    "- line\n",
    "     - `'-'` Solid\n",
    "     - `':'' Dotted\n",
    "     - `'--'` Interrupted\n",
    "- Color\n",
    "     - `'k'' Black\n",
    "     - `'r'' Red\n",
    "     - `'g'' green\n",
    "     - `'b'' Blue\n",
    "     - `'y'' yellow\n",
    "    \n",
    "Please consider reading the matplotlib documentation to explore the full feature set.\n",
    "\n",
    "In the following cell we are reading in values using pandas from `.csv` file, which stands for comma separated values.\n",
    "then we plot those values as a scatter plot using matplotlib. The details of how pandas works are not relevant here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "df = pd.read_csv(\"./data/exercise_2/lin.csv\")\n",
    "x_values = df[\"X\"]\n",
    "y_values = df[\"Y\"]\n",
    "plt.figure()\n",
    "plt.scatter(x_values, y_values, marker=\".\", color=\"red\", label=\"raw values\")\n",
    "plt.xlabel(\"x_values\")\n",
    "plt.ylabel(\"y_values\")\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Task 3\n",
    "\n",
    "In the following task we are using linear algebra to implement linear regression from scratch.  \n",
    "You have probably all used linear regression before.  \n",
    "If we have some data and we assume that it can be described using a linear equation of this sort:\n",
    "$$\n",
    "y = a \\cdot x + b\n",
    "$$\n",
    "We can reformulate this equation using linear algebra, the values for y are packed into a vector \n",
    "$\\mathbf{y} =  \\begin{bmatrix}\n",
    "           y_{1} \\cr\n",
    "           y_{2} \\cr\n",
    "           \\vdots \\cr\n",
    "           y_{m}\n",
    "         \\end{bmatrix}$, $a$ and $b$ are packed into a vector, commonly referred to as $\\mathbf{\\beta}  =  \\begin{bmatrix}\n",
    "           \\beta_{0} = b \\cr\n",
    "           \\beta_{1} = a \n",
    "         \\end{bmatrix}$.  \n",
    "The x values are then added to a matrix \n",
    "$\\mathbf{X} =  \\begin{bmatrix}\n",
    "           1 & x_1 \\cr\n",
    "           1 & x_2 \\cr\n",
    "          1 &  \\vdots \\cr\n",
    "          1 & x_m\n",
    "         \\end{bmatrix}$, resulting in the concise formulation:  $\\mathbf{y} = \\beta \\mathbf{X}$. \n",
    "        "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using our data points $(x_i, y_i)$ will lead in most cases to an overdetermined linear equation problem.  \n",
    "In order to solve this problem, we need to define how we get our optimal $\\beta$-values.  \n",
    "In most cases, the error functions is defined by the least squares estimation, meaning the square of the difference between the predicted value $ \\bar{y_i} = \\vec{\\beta} \\cdot \\vec{x_i}$ and the observed value $y_i$ should be minimal: $ \\min{(\\bar{y_i} - y_i)^2} $.  \n",
    "After some derivation we get a linear system of equations, under the form $\\mathbf{A} \\beta = \\mathbf{B}$.  \n",
    "These sort of equations can be solved by numpy using `np.linalg.solve(A, B)`.  \n",
    "For the least squares loss function, we get the following expressions for $ \\mathbf{A}$ and $ \\mathbf{B}$:\n",
    "\n",
    "$$\n",
    "\\mathbf{A} =  \\mathbf{X}^{\\mathsf{T}}  \\mathbf{X} \\newline\n",
    "\\mathbf{B} =  \\mathbf{X}^{\\mathsf{T}}  \\mathbf{y} \n",
    "$$\n",
    "where: $\\mathbf{X}^{\\mathsf{T}}$ is the transpose of $\\mathbf{X}$, you can compute it using the `.transpose()` method with a matrix.  \n",
    "\n",
    "Use the `x_values` and `y_values` variables defined above, to define two numpy arrays `X` and `Y` with the necessary shape.  \n",
    "Based on these arrays and the matrix-matrix multiplication seen above, compute the matrices $\\mathbf{A}$ and $\\mathbf{B}$ as defined above.  \n",
    "Plug these into the solve function and get back an array containing the intercept $b$ anf the slope $a$.  \n",
    "Using these values computed using your linear regression model, plot them together with the input data in one plot. \n",
    "\n",
    "<u> Make sure that plots are formatted properly (Axis label, plotted an reasonable scale, labeled data, legend, etc.). </u>  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Additional Task 4 (optional)\n",
    "\n",
    "The linear regression model seen above can also be applied to non linear equations, like $\\beta_2 x^2 + \\beta _1 x + \\beta_0 = y$. We need to use the general form for the matrix $\\mathbf{X}$ for a $d$-dimensional input $\\vec{x_i}$:  \n",
    "$$\n",
    "\\mathbf{X} = \n",
    "\\begin{bmatrix}\n",
    "        1 & x_{1,1} & \\ldots &  x_{1,d} \\cr\n",
    "        1 & x_{2,1} & \\ldots &  x_{2,d} \\cr\n",
    "        1 &  \\vdots & \\ddots &  \\vdots \\cr\n",
    "          1 & x_{m,1} & \\ldots &  x_{m,d}\n",
    "\\end{bmatrix}\n",
    "$$\n",
    "\n",
    "The rest of the procedure as well as the formulation for $\\mathbf{A}$ and $\\mathbf{B}$ is identical, `solve(A, B)` should return an array with three entries. Can you guess which physical model was used to create the data?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "df = pd.read_csv(\"./data/exercise_2/square.csv\")\n",
    "x_values = df[\"X\"]\n",
    "y_values = df[\"Y\"]"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.5"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
