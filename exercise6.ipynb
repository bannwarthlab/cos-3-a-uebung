{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 6\n",
    "\n",
    "---\n",
    "Submission until 26/11/2024 12:00 p.m.\n",
    "\n",
    "<span style=\"color:red\">You will need to install pyscf for this exercise manually (`pip install pyscf`) in the corresponding environment. </span>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### Conformer sampling with CREST\n",
    "\n",
    "`CREST` stands for Conformer Rotamer Ensemble Searching Tool.[(1)](https://doi.org/10.1063/5.0197592)[(2)](https://doi.org/10.1039/C9CP06869D)  \n",
    "It was developed mainly in Bonn (with some contributions from our group as well) and is still actively developed by a group of developers headed by Phllipp Pracht.  \n",
    "As you have learned in the lecture `CREST` uses meta-dynamics calculations with a RMSE based bias potential to push the dynamics simulation towards unseen regions of the potential energy surface.[(3)](https://doi.org/10.1021/acs.jctc.9b00143)  \n",
    "For an overview of the functionality provided by `CREST`, please refer to the documentation or papers referenced above.  \n",
    "The conformer search algorithm is based on a series of MTD calculations with subsequent geometry optimizations and a routine to filter out duplicate conformers and rotamers.  \n",
    "These calculations are by default run using a semiempirical tight binding method ([GFN2-xTB](https://pubs.acs.org/doi/10.1021/acs.jctc.8b01176)), with additional options to use solvation models etc.   \n",
    "After a sucessful CREST run, the user is given an ensemble of conformers (`crest_conformers.xyz`), which usually contains multiple geometries.  \n",
    "\n",
    "Commonly, the conformer geometries are then reoptimized using a method providing more accurate energies than GFN2-xTB, like DFT methods with small/moderately-sized basis sets.  \n",
    "These steps were already prepared by us and we provide the DFT-optimized ensemble of structures.  \n",
    "If you would like to use `CREST` yourself, it is installed on the RWTH HPC system and can readily be used by first loading it with `module load crest` and then using either the command-line options or the new `toml`-file input (version 3.0 and higher).  \n",
    "Since CREST is open-source software, you can also pull the source code yourself and compile it for your local system or use precompiled binaries from [github](https://github.com/crest-lab/crest).  \n",
    "For some runtypes, even in new versions of CREST, the `xtb` binary is still needed. Here you find an implementation of GFN2-xTB. On most HPC systems, this is already taken care of by the dependency functionality of the modules.  \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "57vzlqbm2RBL"
   },
   "source": [
    "### Task 1\n",
    "\n",
    "As a first task, we want to estimate the population of the conformers of $n$-Butane (`data/exercise_6/butane_ensemble.xyz`) using electronic energy differences.\n",
    "\n",
    "Create a function to parse the conformers from the given ensemble file and compute the Boltzmann weights ($T = 298.15 K$) of these conformers according to eq. 1. Use the electronic gas phase energies at the GFN2-xTB theory level $E_{\\mathrm{gas},i}^\\mathrm{GFN2}$, which are stored in the comment line of the respective `xyz` section in the ensemble file.\n",
    "\n",
    "Explain (one sentence), why Boltzmann weights should be computed using relative energies instead of absolute values.\n",
    "\n",
    "$$\n",
    "\\begin{align}\n",
    "    \\omega_i = \\frac{\\exp{\\frac{-(E_i - E_{\\text{ref},i})} {k_BT}}} {\\sum_j^{N_{\\text{mol.}}} \\exp{\\frac{-(E_j - E_{\\text{ref},j})} {k_BT}}}\n",
    "\\end{align}\\tag{1}\n",
    "$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [],
   "source": [
    "import scipy.constants as sc\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the image below, calculated gas phase entropies of small alkanes are compared against experimental values from the [NIST database](https://webbook.nist.gov/).\n",
    "\n",
    "<img src=\"./images/exercise_6/gasphase_entropies.png\" alt=\"gas phase entropies alkanes\" width=\"70%\">\n",
    "\n",
    "As can be seen, $S_\\mathrm{tot}$ for molecules in gas phase is a quantity that can be computed with overall good agreement to the experiment. \n",
    "For $n$-Butane, larger deviations with respect to the experimental $S$ are observed, when compared to its structural isomer iso-Butane. \n",
    "\n",
    "Give a brief explanation for the observed differences between these isomers.\n",
    "\n",
    "Implement functions to compute the translational, rotational and vibrational entropy based on the particle-in-a-box, rigid-rotor and harmonic oscillator models respectively. You can use existing code/expressions, if the source is cited appropriately. Use these expressions and the harmonic frequencies (e.g. `0000.freq` for the first conformer of the ensemble) to compute the gas phase entropy of $n$-Butane at different levels of sophistication:\n",
    "\n",
    "a) Using only the lowest electronic energy conformer\n",
    "\n",
    "b) Using the Boltzmann-average between the conformers\n",
    "\n",
    "Briefly comment on the size of the respective T,R and V contributions to $S$. Discuss potential weaknesses of the chosen models (max. 5 sentences). Comment on the differences between the results in a) and b).\n",
    "\n",
    "The [Gibbs-Shannon entropy](https://en.wikipedia.org/wiki/Entropy_(statistical_thermodynamics)) $S_\\mathrm{GS}$ (eq. 2) can be used to compute the conformational entropy:\n",
    "$$\n",
    "S_\\mathrm{GS} = -k_\\mathrm{B}\\sum_i^{N_\\mathrm{conf}} \\omega_i \\ln(\\omega_i) \n",
    "$$\n",
    "\n",
    "Implement a function to compute the GS entropy and add this to the gas phase entropy of $n$-Butane. Explain your findings in regard to the correspondence with the experiment.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "BjzWZnkeLCw4"
   },
   "source": [
    "### Task 2: Boltzmann weights of charged/uncharged species in solvent and gas-phase\n",
    "\n",
    "In the following exercise we are combining our knowledge about implicit solvation models and Boltzmann weights.  \n",
    "Polypeptides can be present in their neutral form and zwitterionic form depending on the environment in which they are.  \n",
    "To investigate which form is dominant, we are computing the Boltzmann weights in gas-phase and in solution as modeled by an implicit solvation model.\n",
    "\n",
    "The solvation free energy could be computed by the generalized Born model as we did in the last exercise, however we want to use `pyscf` to compute the singlepoint energies.  \n",
    "`pyscf` has implementations for various implicit solvation models (take a look at their [documentation](https://pyscf.org/user/solvent.html)).\n",
    "We will be using the conductor-like polarizable continuum model (C-PCM) for the electrostatic contributions. The `pyscf` documentation provides some outdated examples on how to use C-PCM for singlepoint calculations. Please use the syntax outlined in the code snippet below to set up your calculations with sovlation. \n",
    "\n",
    "Read in the geometries given in XYZ standard, where `Ala.xyz` ist the neutral and `Ala_zwitter.xyz` the zwitter-ionic species.\n",
    "Those geometries underwent a conformer search with CREST and DFT geometry optimization with the r2SCAN-3c composite method.\n",
    "\n",
    "Compute the gas phase energies for both geometries using the B3-LYP DFT functional and def2-SVP basis set.\n",
    "\n",
    "Compute the Boltzmann weights for both species based on these energies at $T = 298.15 K$.\n",
    "\n",
    "Repeat the process, but this time compute the energies involving C-PCM for water ($\\epsilon_r$ = 78.3).\n",
    "Compute the Boltzmann weights in the solvent phase as well.\n",
    "\n",
    "How do the Boltzmann weights differ in both media? How can you explain your observation?\n",
    "Are we considering all contributions to the Gibbs free energy for our Boltzmann weights?\n",
    "\n",
    "<u> Hint:</u> Place all calculations with PySCF in one cell and assign the energy values to variables that you use in another cell to compute the Boltzamnn weights. Remember to set the correct dielectric constant in the calculations involving C-PCM."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Example for C-PCM in pyscf\n",
    "# from pyscf.solvent import pcm\n",
    "# mol = pyscf.M(...)\n",
    "# mf = mol.RKS(xc=\"b3lyp\")\n",
    "# mf.kernel()\n",
    "# pcm_ = pcm.PCM(mf)\n",
    "# pcm_.eps = 78.3\n",
    "# mf_pcm = mol.RKS(xc=\"b3lyp\").PCM(pcm_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### Example 2 for C-PCM in pyscf\n",
    "# from pyscf.solvent import PCM\n",
    "# mol = pyscf.M(...)\n",
    "# mf = mol.RKS(xc=\"b3lyp\")\n",
    "# mf = PCM(mf)\n",
    "# mf.with_solvent.eps = 78.3\n",
    "# mf.kernel()"
   ]
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "exvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
