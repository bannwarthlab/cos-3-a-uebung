### Contains functions to use in exercise 9 ### 
# Ref: https://github.com/PolymerTheory/MDFromScratch

import numpy as np
import os
import scipy.constants as sc
from typing import Callable, List, Tuple, Dict

# Unit conversion to "atomic-scale" units:
# atomic mass unit (amu)
# Angstrom (A),
# picoseconds (ps)
# elemental charge (e)

kg2amu = 1/sc.atomic_mass

kb_at = sc.k * kg2amu * 1e20 * 1e-24 # J/mol/K = kg * m2 * s-2 * K-1 *mol-1 -> au* A2 * ps-2 * mol-1 * K-1

# Prefactor of Coulomb law: 1/(4 pi eps0) # C-2 * s-2 * kg * m3 -> e-2 * ps-2 * amu * A3
kelec_at = 1/(4 * np.pi * sc.epsilon_0) * sc.electron_volt**2  * 1e-24 * 1e30 * 1/sc.atomic_mass


### Helper function to build model system from input parameters and to run MD simulation in a more concise way ###
def build_model(nmol : int, bdist : float, kbond : float, theta : float, ktheta : float, ms, qs) -> tuple[np.array, np.array, np.array, np.array, np.array, np.array]:
    
    # Initialize a matrix of bonds
    bonds = np.array([
        [3 * i, 3 * i + 1, bdist, kbond] for i in range(nmol // 3)
    ] + [
        [3 * i + 1, 3 * i + 2, bdist, kbond] for i in range(nmol // 3)
    ])

    # Initialize a vector of angles
    angles = np.array([
        [3 * i, 3 * i + 1, 3 * i + 2, theta, ktheta] for i in range(nmol // 3)
    ])

    # Atom types in groups of three
    types = [t for i in range(nmol // 3) for t in (0, 1, 0)]

    # Molecule labels
    molecules = [i for i in range(nmol // 3) for _ in range(3)]

    # Mass and charge arrays
    masses = np.array([ms[types[j]] for j in range(nmol)])
    charges = np.array([qs[types[j]] for j in range(nmol)]) 

    return {
        "bonds" : bonds,
         "angles": angles, 
         "attypes": types, 
         "mols": molecules, 
         "masses": masses, 
         "charges": charges
    }

def run_MD(model : dict, 
           xin : np.array, 
           vin : np.array,
            dt : float, 
            L : np.array,
            sigma : np.array,
            epsilon : np.array, 
            simT : float = 300, 
            nstep : int = 200,
            nskip : int = 10, 
            dumpdir : str = "_dump_md", 
            doPBC : bool = False
            ) -> None:
    """ Run empirically modified MD simulation for water model.

        Args:   
            model (dict): Dictonary defining properties of the model (see above fct.).
            xin (np.array): Intial particle positions.
            vin (np.array): Initial particle velocities.
            dt (float): Time step for propagation.
            L (np.array): Simulation box for constraints.
            sigma (np.array): Sigma pair-parameters of LJ pot.
            epsilon (np.array): Epsilon pair-parameters of LJ pot.
            simT (float): Simulation temperature for MD. Default 300 K.
            nstep (int, optional): Number of steps in MD. Default 10.
            nskip (int, optional): Number of dumps for snapshots. Default 10.
            dumpdir(str): Directory to place snapshot files in. Default "_dump_md"
            doPBC (bool): Whether to apply PBC or not. Default False.

        Returns: 
            None
       """
    if nskip > nstep:
        raise ValueError("More dumps requested than steps.")

    required_keys = ["bonds", "angles", "attypes", "mols", "masses", "charges"]
    missing_keys = [key for key in required_keys if key not in model]
    if missing_keys:
        raise AttributeError(f"Model dict is missing required attributes: {', '.join(missing_keys)}.")
    
    # Set a directory to dump MD snapshots into
    try: #make directory if it does not exist
        os.mkdir(dumpdir)
    except:
        pass

    x_curr, v_curr = xin.copy(), vin.copy()

    for i in range(nstep):
        # Update particle velocities
        v_new, a = v_update(x_curr, v_curr, dt, 
                            model["bonds"], 
                            model["angles"], 
                            sigma, 
                            epsilon, 
                            model["charges"], 
                            model["mols"], 
                            model["attypes"], 
                            model["masses"]
                            ) 
        
        # Rescale velocities to temperature
        v_ = rescale_T(v_new, simT, model["masses"]) 

        # Update particle position
        x_new, v_new = x_update(x_curr, v_, L,dt, PBC = doPBC)  

        x_curr, v_curr = x_new.copy(), v_new.copy()
        # Dump data every 'skip' steps
        if i % nskip == 0:
            # Write .dump file
            dump_ovito(x_new, int(i / nskip), model["mols"], model["attypes"], L, dumpdir)  
            #print(f"Step {int(i / nskip)} saved.")


    return


### Equations to compute energy and forces according to eq. 6 and 7

# Computes the bond length potential according to first term in eq. 6

def U_BE(x : np.array, bnds : np.array) -> float:
    """ Compute the bond length potential according to first term in eq. 6
    
    Args:
        x (np.array): Positions of the particles.
        bnds (np.array): List of bonds defined within the system.
    
    Returns:
        bps (np.array): Bond energy potential between atom pairs i,j.
    """
    bps=np.zeros(len(x))

    for i in range(len(x)): #loop over all particles
        for j in range(len(bnds)): #check all bonds to see if particle i is bonded
            if bnds[j][0]==i or bnds[j][1]==i:
                if bnds[j][0]==i: #find particle bonded to i
                    ii=int(bnds[j][1])
                else:
                    ii=int(bnds[j][0])
                dr0=bnds[j][2]
                e0 =bnds[j][3]
                dr=x[i]-x[ii]
                dr2=dr*dr
                adr2=sum(dr2)
                adr=np.sqrt(adr2)
                BE=e0*(adr-dr0)**2
                bps[i]+=BE
    return bps


def d_BE(x: np.ndarray, bnds: np.ndarray):
    """ Compute the gradient of the bond length potential.
    
    Args:
        x (np.array): Positions of the particles.
        bnds (np.array): List of bonds defined within the system.
    
    Returns:
        bps (np.array): Bond energy potential between atom pairs i,j.
    """
    n = len(x)
    bps=np.zeros([n,3])
    for i in range(n): #loop over all particles
        for j in range(len(bnds)): #check all bonds to see if particle i is bonded
            if bnds[j][0]==i or bnds[j][1]==i:
                if bnds[j][0]==i: #find particle bonded to i
                    ii=int(bnds[j][1])
                else:
                    ii=int(bnds[j][0])
                dr0=bnds[j][2]
                e0 =bnds[j][3]
                dr=x[i]-x[ii]
                adr = np.linalg.norm(dr)
                dBE=2.0*e0*(adr-dr0)*dr/adr
                bps[i]+=dBE
    return bps

def d_BA(x: np.ndarray, angs: np.ndarray):
    """ Compute the gradient of the bond angle potential.
    
    Args:
        x (np.array): Positions of the particles.
        angs (np.array): List of angles defined within the system.
    
    Returns:
        bps (np.array): Bond energy potential between atom pairs i,j.
    """
    n = len(x)
    aps=np.zeros([n,3])
    for i in range(len(x)): #loop over all particles
        for j in range(len(angs)): #check all bonds to see if particle i is bonded
            a1=int(angs[j][0])
            a2=int(angs[j][1])
            a3=int(angs[j][2])
            if i==a1 or i==a2 or i==a3:
                th00=angs[j][3] #equilibrium angle
                e0 =angs[j][4] #bending modulus
                if i==a1 or i==a2:
                    r1=x[a1]-x[a2] #bond vector 1 (form middle atom to atom 1)
                    r2=x[a3]-x[a2] #bond vector 2 (middle atom to atom 2)
                else:
                    r1=x[a3]-x[a2] #bond vector 1 (form middle atom to atom 1)
                    r2=x[a1]-x[a2] #bond vector 2 (middle atom to atom 2)
                ar1=np.sqrt(sum(r1*r1)) #lengths of bonds
                ar2=np.sqrt(sum(r2*r2))
                dot=sum(r1*r2) #r1 dot r2
                ndot=dot/(ar1*ar2) #normalize dot product by vector lengths i.e. get the cos of angle
                th=np.acos(ndot) #bond angle, theta
                dUdth=-2.0*e0*(th-th00) #-dU/dtheta
                if a1==i or a3==i:
                    numerator=(r2/(ar1*ar2))-(dot/(ar1*ar1*ar1*ar2*2.0))
                    denominator=np.sqrt(1.0-ndot*ndot)
                    dUdr=dUdth*numerator/denominator
                    aps[i]+=dUdr
                if i==a2:
                    denominator=np.sqrt(1.0-ndot*ndot)
                    n1=-(r2+r1)
                    n2=dot*r1/(ar1*ar1)
                    n3=dot*r2/(ar2*ar2)
                    numerator=(n1+n2+n3)/(ar1*ar2)
                    dUdr=dUdth*numerator/denominator
                    aps[i]+=dUdr
    return aps


def U_LJ(x: np.ndarray, 
         i: int, 
         sig: np.ndarray, 
         eps: np.ndarray) -> np.array:
    """ Compute the Lennard Jones potential for particle i.
    
    Args:
        x (np.array): Positions of the particles.
        i (int): Index of particle i.
        sig (np.array): Sigma parameters for LJ potential.
        eps (np.array): Eps. pararmeters for LJ potential.
    
    Returns:
        ljp (float): LJ potential for particle i.

    """

    drv = x - x[i]  # Distance in each dimension to particle i
    drv = np.delete(drv, i, 0)  # Remove self interactions for particle i
    dr = np.linalg.norm(drv, axis=1)  
    r6 = (sig / dr) ** 6
    r12 = (sig / dr) ** 12

    # Compute LJP according to first term in eq. 7
    ljp = 4.0 * eps * np.sum(eps * r6 - eps * r12)
    return ljp


def d_LJ(x: np.ndarray, 
         sig: np.ndarray, 
         eps: np.ndarray,
         mols : np.array,
         atypes : np.array) -> np.array:
    """ Compute gradient of Lennard Jones potential for particle i.
    
    Args:
        x (np.array): Positions of the particles.
        sig (np.array): Sigma parameters for LJ potential.
        eps (np.array): Eps. pararmeters for LJ potential.
        mols (np.array): Definition of the molecules.
        atomtypes (np.array): Vector containing atom types to derive empirical parameters.
    
    Returns:
        ljp (float): LJ potential for particle i.

    """
    n = len(x)
    dLJP = np.zeros(n)
    #print(n)
    #print("AB", eps, eps.shape)    

    #eps_ = np.array([])
    #sig_ =

    dLJP = np.zeros([n,3])
    for i in range(n):
        eps_ = eps[atypes[i]]
        sig_ = sig[atypes[i]]
        

        sigma_p = np.delete(np.array([sig_[atypes[j]] for j in range(n)]),i)
        #print(sigma_p, sigma_p.shape)
        eps_p =np.array([eps_[atypes[j]] for j in range(n)])
        #print(eps_p.shape)
        for ii in range(n): #ignore atoms in the same molecule
            if mols[i]==mols[ii]:
                eps_p[ii]=0
        ep=np.delete(eps_p,i)

        drv=x-x[i] #distance in each dimension
        drv=np.delete(drv,i,0) #remove ith element (no self LJ interactions)
        dr=np.linalg.norm(drv, axis=1) #absolute distance

        r8 = ep*(sigma_p**6)*(1.0/np.array(dr))**8
        r14= 2.0*ep*(sigma_p**12)*(1.0/np.array(dr))**14
        r8v = np.transpose(np.transpose(drv)*r8)
        r14v = np.transpose(np.transpose(drv)*r14)
        r8vs = np.sum(r8v,axis=0)
        r14vs = np.sum(r14v,axis=0)

        dLJP[i] = 24.0*(r14vs-r8vs)
    return dLJP


# Coulomb force
def Coulomb(x: np.ndarray,
             chrgs: np.ndarray,
               mols : np.array) -> np.array:
    """ Compute derivative of the Coulomb potential. """

    Fc = np.zeros([len(x), 3])
    for i in range(len(x)):
        q0=chrgs[i]
        qs= chrgs.copy()
        for j in range(len(x)):
            if mols[i]==mols[j]:
                qs[j]=0.0
        qs=np.delete(qs,i)
        drv=x-x[i] #distance in each dimension
        drv=np.delete(drv,i,0) #remove self interaction of i 
        dr = np.linalg.norm(drv, axis = 1)
        #dr=[np.sqrt(a[0]*a[0]+a[1]*a[1]+a[2]*a[2]) for a in drv] #absolute distance
        r3=q0*qs*kelec_at*((1.0/np.array(dr))**3.0)
        FF =np.transpose(np.transpose(drv)*r3)
        Fc[i] = np.sum(FF,axis=0)
    return Fc


### MD related functions for updating positions, velcoities
### temperature rescaling, applying boundary conditions and
### writing out the snapshots


def x_update(x: np.array, 
             v: np.ndarray, 
             box : np.ndarray, 
             dt: float, 
             PBC: bool = False) -> tuple[np.array, np.array]:
    """ Update positions and apply PBC, if required. 
    
    Args:  
        x (np.array): Positions of the system.
        v (np.array): Velocities of the system.
        box (np.array): Vector defining the box dimensions.
        dt (float): Time step (in ps).
        BC (bool, optional): Logical wheter PBC should be enforced.

    Returns:
        new_x, new_v (np.array): New positions and velocities.

    """
    
    # Update positions based on velocities and given time step
    new_x = x + dt * v

    # Handle periodic boundary conditions (PBC)
    if PBC:  
        new_x %= box
        new_v = np.copy(v)
    else:  # Reflect molecules on box wall
        new_x, new_v = reflectBC(new_x, v, box)
    return new_x, new_v


def v_update(x : np.array,
             v : np.array,
             dt : float,
             bnd : np.array,
             angs : np.array,
             sigmas : np.array,
             epsilons : np.array,
             chrg : np.array,
             molcules : np.array,
             atomtypes : np.array,
             masses : np.array) -> tuple[np.array, np.array]:
    """ Update velocities based on the computed nuclear forces. 
    
    Args:  
        x (np.array): Positions of the system.
        v (np.array): Velocities of the system.
        dt (float): Time step (in ps).
        bnd (np.array): Bonds defined for the system.
        angs (np.array): Bond angles defined for the system.
        sigmas (np.array): Matrix containing LJ pair parameters sigma.
        epsilons (np.array): Matrix containing LJ pair parameters epss.
        chrg (np.array): Atomic charges of the systems particles.
        molecules (np.array): Definition of molecules.
        atomtypes (np.array): Atom types of particles.
        masses (np.array): Molecular mass of the particles.

    Returns:
        new_v, a (np.array): New velocities and acceleration of particles.

    """    

    #calculate acceleration as F = - dU/dr where U is the sum of equation 6 and #7.
    F = -d_BE(x, bnd) #Bond potential force

    F-= d_BA(x, angs) #Angle force

    F -= d_LJ(x, sigmas, epsilons, molcules, atomtypes) # LJ part

    F -= Coulomb(x,chrg, molcules)  #Coulomb

    a = np.transpose(np.transpose(F) / masses) #Force->acceleration
    
    #update velocity
    newv = v + dt * a
    return newv, a

def rescale_T(v: np.ndarray, 
             T: float, 
             masses: np.ndarray,
             slowscale : bool = False) -> np.array:
    """ Rescale velocities based on current thermodyn. temperature - thermostat.
    
    Args:
        v (np.array): Velocities of the system.
        T (float): Thermodyn. temperature (in K).
        masses (np.array): Masses of the system.
        slowscale (bool): Whether slow temperature scaling should be involved. Default False.
    """
    E_kin = 0.5 * np.sum(masses * np.transpose(v * v))
    avEkin = E_kin / len(v)
    Tnow = (2.0 / 3.0) * avEkin / kb_at
    lam = np.sqrt(T / Tnow)
    if slowscale:
        lam = (lam - 1.0) * 0.5 + 1.0  # Update slowly
    vnew = lam * v
    return vnew

# Make particles reflect off boundaries
def reflectBC(x: np.ndarray, v: np.ndarray, L: np.ndarray):
    """ Perform particle reflection on box L.
    
    Args:
        x (np.array): Positions of the system.
        v (np.array): Velocities of the system.
        L (np.array): Vector defining the box volume.

    Returns:
        new_x (np.array): Updated (reflected) positions.
        new_v (np.array): Updated (reflected) velocities.
    """
    
    # Generate copies of positions and velocities
    new_x = np.copy(x)
    new_v = np.copy(v)

    for i in range(len(x)): # Iterate over all particles
        for j in range(len(L)): # Iterate over cart. directions

            # If we were to leave the box at the lower bound, reflect the particle direction and point velocity component inwards 
            if new_x[i, j] < 0:
                new_x[i, j] = -new_x[i, j]
                new_v[i, j] = np.abs(v[i, j])

            # Reflection if the particle would leave upper part of bounds
            if new_x[i, j] > L[j]:
                new_x[i, j] = 2.0 * L[j] - new_x[i, j]
                new_v[i, j] = -np.abs(v[i, j])

    return new_x, new_v


def dump_ovito(x: np.array, t: int, mols: np.array, tp: np.array, L: np.array, outdir: str):
    """
    Write positions and masses of the particles to a .dump file in a specific format.

    Args:
        x (np.array): The positions of the particles.
        t (int): The current timestep (used in the file name).
        mols (np.array): The molecule IDs for each particle.
        tp (np.array): The particle types (0 for H, 1 for O).
        L (np.array): The simulation box. 
        outdir (str): The directory where the .dump file will be saved.
        mm (np.array): The masses of the particles.
    """
    fname = f"{outdir}/t{t}.dump"
    
    with open(fname, "w") as f:

        f.write("ITEM: TIMESTEP\n")

        f.write(f"{t}\n") #time step

        f.write("ITEM: NUMBER OF ATOMS\n")

        f.write(f"{len(x)}\n") # number of atoms

        f.write("ITEM: BOX BOUNDS pp pp pp\n") #pp = periodic BCs
        f.write(f"0 {L[0]}\n0 {L[1]}\n0 {L[2]}\n")

        f.write("ITEM: ATOMS id mol type x y z\n")

        for i in range(len(x)):
            f.write(f"{i} {mols[i]}\t{tp[i]}\t{x[i][0]:.6f}\t{x[i][1]:.6f}\t{x[i][2]:.6f}\n")
    f.close()




