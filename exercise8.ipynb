{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "IuYhFXnk2RBH"
   },
   "source": [
    "# COS 3a Exercise 8\n",
    "\n",
    "---\n",
    "Submission until 10/12/2024 12:00 p.m.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "id": "2qhCnjYR2RBK"
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import pandas as pd\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy as sc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "gXDfpmuL2RBK"
   },
   "source": [
    "## Tutorial\n",
    "\n",
    "### (Electronic) Circular Dichroism (ECD)\n",
    "\n",
    "ECD spectroscopy can be used to distinguish between two enantiomers, since the enantiomers' spectra show opposing signs.  \n",
    "However, without separation of both enantiomers and a subsequent recording of separate spectra, it is not easy to determine, which absolute configuration is present from an experimental point of view.  \n",
    "Therefore, simulation of such spectra by means of quantum-chemical methods can help assigning the absolute configuration of a sample.  \n",
    "Consepts of a computational workflow to simulate optical spectra has been partially covered in the lecture and we will shortly glance over it again.  \n",
    "\n",
    "### Multilevel Workflow for Spectra Simulation\n",
    "\n",
    "First, we start with a conformer analysis, using tools such as CREST combined with efficient electronic structure methods as GFN2-xTB, potentially using an implicit solvation model.  \n",
    "The conformer ensemble is then reoptimized using a so-called \"low-cost\" DFT method to sort out duplicates and further reduce the energy interval of relevant structures in the conformer ensemble.  \n",
    "At this point, our geometries are fixed and we will further refine the level of theory to compute Boltzmann weights and spectroscopic properties of interest. For ECD spectra, the quantity proportional to the \"intensity\" is the rotor strength, $R$.\n",
    "\n",
    "<img src=\"./images/exercise_8/multilevel.png\" alt=\"multilevel workflow\" width=\"30%\">\n",
    "\n",
    "\n",
    "### Simulation of ECD spectra\n",
    "\n",
    "The first step to simulate ECD spectra is to choose a level of theory to describe the excited states.\n",
    "The workhorse for most electronic structure calculations in the ground state is density functional theory (DFT), since it shows a good balance between accuracy and computational effort.  \n",
    "DFT has been extended to compute time dependent properties, establishing TD-DFT and resulting in the non-hermitian equations shown below (will be discussed in more detail in the following lectures).\n",
    "\n",
    "$\\begin{pmatrix}\n",
    "    \\mathbf{A} & \\mathbf{B} \\\\\n",
    "    \\mathbf{B^*} & \\mathbf{A^*} \\\\\n",
    "    \\end{pmatrix} \\begin{pmatrix}\n",
    "    \\vec{X} \\\\\n",
    "    \\vec{Y} \\\\\n",
    "    \\end{pmatrix} = \\begin{pmatrix}\n",
    "    \\vec{X} \\\\\n",
    "    \\vec{Y} \\\\\n",
    "    \\end{pmatrix}\n",
    "    \\begin{pmatrix}\n",
    "    \\omega & 0 \\\\\n",
    "    0 & -\\omega \\\\\n",
    "    \\end{pmatrix} \n",
    "$\n",
    "\n",
    "Several approximations to this formalism have been proposed, one of them neglects the $\\mathbf{B}$-matrices giving the Tamm-Dancoff approximation (TDA).  \n",
    "TDA extends the applicability of this method to larger systems than TD-DFT. However, the computation of the ab-initio electron repulsion integrals remains computationally prohibitive for systems >100 atoms.  \n",
    "Grimme and coworkers proposed to further approximate these methods using semi-empirical formulations of the terms in the $\\mathbf{A}$ and $\\mathbf{B}$-matrices.  \n",
    "These methods have been named sTD-DFT [[1]](https://doi.org/10.1063/5.0020543), and sTDA-DFT [[2]](https://doi.org/10.1063/1.4959605) accordingly.  \n",
    "Using these methods, we can also evaluate the excited states of large systems and thereby compute their optical spectra.\n",
    "\n",
    "The electronic $\\vec{\\mu}_{nm}$ and magnetic $\\vec{m}_{mn}$ transition dipole moments are needed to compute the rotor strength $R$:\n",
    "\n",
    "$\n",
    "R_{nm} = -Im(\\vec{\\mu}_{nm}\\vec{m}_{mn}) \n",
    "$\n",
    "\n",
    "For chiral molecules the product $\\vec{\\mu}_{nm}\\vec{m}_{mn}$ becomes non-zero and the rotational strength becomes non-vanishing.\n",
    "\n",
    "The rotational strength is proportional to the difference of the molar absorption coefficient of left and right circularly polarized light $\\Delta \\varepsilon = \\varepsilon_L - \\varepsilon_R $.  \n",
    "Unit conversions are quite tedious as it involves the computation of an integral. Therefore we will use the rotational strength $R_{nm}$ directly instead of $\\Delta \\varepsilon$ to represent the ECD spectrum.  \n",
    "\n",
    "The main focus of this exercise will be to compute a continuous spectrum from the discrete values for $R_{nm}$ that we get from our excited calculation with the (s)TD-DFT method.\n",
    "\n",
    "The values that we have provided come from a cooperation that our group did with Prof. Bach from the TU Munich.[[3]](https://doi.org/10.1021/jacs.2c02511)  \n",
    "For the following reaction, a single enantiomer was detected. We were asked to compute the ECD spectra and specific angle of rotation to determine the absolute configuration of the product.\n",
    "\n",
    "<img src=\"./images/exercise_8/reaction.png\" alt=\"multilevel workflow\" width=\"30%\">"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Task 1\n",
    "\n",
    "After DFT reoptimization, we found two conformers that are accessible at room temperature.  \n",
    "We provide the output of the excited state computation as written by the `stda` program using the sTD-DFT formalism, as `tda.dat` for both conformers.  \n",
    "For the ground-state, we used the BHLYP exchange-correlation functional with an def2-SVP basis set. \n",
    "This output contains 13 lines - a header which you can skip for parsing the file - followed by the actual data.  \n",
    "The data is structured in columns in the following way:\n",
    "\n",
    "1. column: index of the excitation\n",
    "2. column: excitation energy in eV\n",
    "3. column: oscillator strength in the length-formalism\n",
    "4. column: oscillator strength in the velocity-formalism\n",
    "5. column: rotational strength in the length-formalism\n",
    "6. column: rotational strength in the velocity-formalism\n",
    "\n",
    "Write a function to read in the `tda.dat` files using pandas, returning the excitation energies and rotational strength **in the velocity-formalism** as numpy arrays.\n",
    "For simplicity, we named the folders `0000` and `0001`.  \n",
    "Using this function, plot the rotational strength as a function of the excitation energy, using `plt.bar()` and a width of 0.01 eV.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "id": "UYUQutUM2RBR",
    "outputId": "ed71163e-dc07-4b1b-81e7-d036d537504e"
   },
   "outputs": [],
   "source": [
    "# Write your solution here\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "bmnQbHvvnBQZ"
   },
   "source": [
    "## Task 2 \n",
    "\n",
    "The so-called \"sticks\" that we have plotted in the previous task clearly differ from experimental spectra in that they are not continous over a desired energy range.  \n",
    "The most common technique to more adequately mimic the experimental spectrum is to broaden the calculated sticks using Gaussian functions. More precisely, we want to compute a convolution of these Gaussians. In principle other lineshape functions (e.g. Lorentzians) can be used too.\n",
    "\n",
    "The Gaussian function should have two parameters: $x_0$, the center of the distribution (here: the distribution energy), and the width of the Gaussian $\\sigma$.  \n",
    "Usually, the width is manually set to reproduce the experimental spectrum, or to a value that appears to give a suitable spectral \"resolution\". One often used quantity connected to the width of the Gaussian is the \"full width at half maximum\" (FWHM):\n",
    "\n",
    "$$\n",
    "\\sigma = \\frac{\\text{FWHM}}{2\\sqrt{2\\ln{2}}}\n",
    "$$\n",
    "whereas the Gaussian reads:\n",
    "$$\n",
    "g(x, x_0) = \\frac{1}{\\sigma \\sqrt{2\\pi}} \\exp{\\left(-\\frac{(x-x_0)^2}{2\\sigma^2}\\right)} \n",
    "$$\n",
    "Combining this with the rotor strength at a certain excitation energy $\\epsilon$, we get:\n",
    "$$\n",
    "I(\\epsilon) = \\sum_{i}^{n_{\\text{exc.}}} g(\\epsilon, \\epsilon_i, \\text{FWHM}) \\cdot R_i \n",
    "$$\n",
    "\n",
    "The spectrum is computed by evaluating a Gaussian distribution on the interval of energies $\\epsilon$ for every excitation. \n",
    "This values are then added up as shown by the equation above.\n",
    "\n",
    "\n",
    "Write a function that computes the spectrum based on the following parameters:\n",
    "- minimum, maximum energy of the interval\n",
    "- number of values in the interval, \n",
    "- excitation energies of the sticks\n",
    "- rotational strengths (in au)\n",
    "- FWHM  \n",
    "\n",
    "Compute the spectrum for both conformers, for an interval $\\epsilon \\in [3.8, 10.4]$ eV and 1000 points. Use a FWHM value of 0.2 eV.  \n",
    "Plot the spectra together with the corresponding sticks in a figure.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "id": "d-_-a_NOnBQa",
    "outputId": "0a50fa6e-0047-4bde-fb20-4cd374477713"
   },
   "outputs": [],
   "source": [
    "# Write your solution here"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "id": "Fa9NvRY-nBQb"
   },
   "source": [
    "## Task 3\n",
    "\n",
    "From the previous tasks, we have observed that for this molecule the ECD spectrum shows different signs depending on the conformer.  \n",
    "This also shows that it is very important to do a conformer search when computing properties (especially signed properties as $R$!). Without consideration of different conformers, the simulated spectrum might end up with exactly the wrong sign, hence leading to misconceptions when compared to experimental data.  \n",
    "The spectra of our conformers are scaled using Boltzmann weights based on their free Gibbs energies.  \n",
    "We will be using the Boltzmann weights computed during the above mentioned project.  \n",
    "The theory levels used for the individual contributions can be found in the supporting information of the paper.  \n",
    "We provide them as a `boltzmann.csv` file. This file can be read in using `pandas.read_csv()` to get the Boltzmann weights directly.\n",
    "Differences between the spectra in this exercise and the publication are due to additional geometry sampling but will not be discussed further.\n",
    "\n",
    "To compare with experiment data, usually the spectrum is shown in nm rather than eV.  \n",
    "\n",
    "\n",
    "Write a function to convert eV to nm and one to convert from nm to eV. Use the `scipy.constants` module for the conversion! \n",
    "\n",
    "\n",
    "When plotting a spectrum it is important to execute the Gaussian convolution in the energy unit and then convert to wavelengths.  \n",
    "\n",
    "\n",
    "Write a function to convert a spectrum in nm, the emin and emax parameters are given in nm, the width will remain at 0.2 eV.  \n",
    "Remember that energy is proportional to the inverse of the wavelength, so `emin` will be 300 nm and `emax` 200 nm.  \n",
    "\n",
    "To consider the Boltzmann weights, the rotor strength is multiplied by the Boltzmann weight of the according species.\n",
    "\n",
    "\n",
    "Plot the spectrum of conformer `0000` and `0001` as well as the Boltzmann-weighted spectrum in one plot. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Write Your solution here\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Additional Task 4 (optional)\n",
    "\n",
    "Show that a Gaussian convolution in the wavelength domain gives another spectrum than the conversion in the energy domain.  \n",
    "Take 10 nm for the FWHM argument and scale the spectrum given by 50. \n",
    "Plot the ECD spectrum between 150nm and 300nm.\n",
    "\n",
    "Discuss where the differences are coming from."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "colab": {
   "provenance": []
  },
  "kernelspec": {
   "display_name": "exvenv",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.13.0"
  },
  "orig_nbformat": 4
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
